package be.kdg.JeugdbewegingProject.logging;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * Tim Delrue
 * 06/01/2022
 */
public class SmallLogFormatter extends Formatter {
    @Override
    public String format(LogRecord record) {
        String result = "";
        LocalDateTime ldt = LocalDateTime.ofInstant(record.getInstant(), ZoneId.systemDefault());
        String message = MessageFormat.format(record.getMessage(), record.getParameters());
        Level level = record.getLevel();

        result += ldt.format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS"));
        result += " Level: " + level.toString();
        result += " melding: " + message;
        result += "\n";

        return result;
    }
}
