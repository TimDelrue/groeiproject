package be.kdg.JeugdbewegingProject;

import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import be.kdg.JeugdbewegingProject.model.Jeugdbewegingen;
import be.kdg.JeugdbewegingProject.model.Type;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDate;
import java.util.logging.LogManager;

/**
 * Tim Delrue
 * 06/01/2022
 */
public class Demo_5 {
    public static void main(String[] args) {
        URL configURL = Demo_5.class.getResource("/logging.properties");
        if (configURL != null) {
            try (InputStream is = configURL.openStream()) {
                LogManager.getLogManager().readConfiguration(is);
            } catch (IOException e) {
                System.err.println("Configuratiebestand is corrupt");
            }
        } else {
            System.err.println("Configuratiebestand NIET GEVONDEN");
        }


        Jeugdbeweging j1 = new Jeugdbeweging("pleplo", 5, 100, 14, Type.CHIRO, LocalDate.of(1989, 10, 10));
        Jeugdbeweging j2 = new Jeugdbeweging("pleplo", 300, 5, 14, Type.CHIRO, LocalDate.of(1989, 10, 10));
        Jeugdbeweging j3 = new Jeugdbeweging("pleplo", 300, 100, 5, Type.CHIRO, LocalDate.of(1989, 10, 10));
        Jeugdbeweging j4 = new Jeugdbeweging("pleplo", 300, 100, 14, Type.CHIRO, LocalDate.of(1500, 10, 10));

        Jeugdbeweging j10 = new Jeugdbeweging("pleplo", 300, 100, 14, Type.CHIRO, LocalDate.of(1989, 10, 10));
        Jeugdbeweging j11 = new Jeugdbeweging("chiro lore", 300, 100, 14, Type.CHIRO, LocalDate.of(1989, 10, 10));
        Jeugdbeweging j12 = new Jeugdbeweging("chiro centurm", 300, 100, 14, Type.CHIRO, LocalDate.of(1989, 10, 10));
        Jeugdbeweging j13 = new Jeugdbeweging("chiro bamischijf", 300, 100, 14, Type.CHIRO, LocalDate.of(1989, 10, 10));
        Jeugdbeweging j14 = new Jeugdbeweging("chiro het bang schaapke", 300, 100, 14, Type.CHIRO, LocalDate.of(1989, 10, 10));
        Jeugdbeweging j15 = new Jeugdbeweging("chiro dol fijn", 300, 100, 14, Type.CHIRO, LocalDate.of(1989, 10, 10));
        Jeugdbeweging j16 = new Jeugdbeweging("chiro walrus", 300, 100, 14, Type.CHIRO, LocalDate.of(1989, 10, 10));

        Jeugdbewegingen jeugdbewegingen = new Jeugdbewegingen();

        jeugdbewegingen.add(j10);
        jeugdbewegingen.add(j12);
        jeugdbewegingen.add(j13);
        jeugdbewegingen.add(j14);
        jeugdbewegingen.add(j15);
        jeugdbewegingen.add(j16);
    }
}
