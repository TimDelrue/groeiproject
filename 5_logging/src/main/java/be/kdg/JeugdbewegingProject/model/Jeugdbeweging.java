package be.kdg.JeugdbewegingProject.model;

import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Tim Delrue
 * 05/10/2021
 */
public class Jeugdbeweging implements Comparable<Jeugdbeweging> {
    private static Logger logger =
            Logger.getLogger("be.kdg.be.be.be.kdg.JeugdbewegingProject.model.Jeugdbeweging");

    private String naam;
    private double oppervlakte;
    private int aantalLeden;
    private int aantalLeiding;
    private Type type;
    private LocalDate datumOprichting;

    public Jeugdbeweging() {
        this.naam = "Onbekend";
        this.oppervlakte = 100;
        this.aantalLeden = 100;
        this.aantalLeiding = 10;
        this.type = Type.SCOUTS;
        this.datumOprichting = LocalDate.of(2000,1,1);
    }

    public Jeugdbeweging(String naam, double oppervlakte, int aantalLeden, int aantalLeiding, Type type, LocalDate datumOprichting) {
        this.setNaam(naam);
        this.setOppervlakte(oppervlakte);
        this.setAantalLeden(aantalLeden);
        this.setAantalLeiding(aantalLeiding);
        this.setType(type);
        this.setDatumOprichting(datumOprichting);
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public double getOppervlakte() {
        return oppervlakte;
    }

    public void setOppervlakte(double oppervlakte) throws IllegalArgumentException{
        if (oppervlakte < 10 || oppervlakte > 10000){
            logger.log(Level.SEVERE,"De oppervlakte (waarde:" + oppervlakte + ") moet tussen 10 en 10000 M² liggen voor jeugdbeweging " + this.naam);
        } else
        this.oppervlakte = oppervlakte;
    }

    public int getAantalLeden() {
        return aantalLeden;
    }

    public void setAantalLeden(int aantalLeden) {
        if (aantalLeden < 10 || aantalLeden > 2500){
            logger.log(Level.SEVERE,"Het aantal leden (waarde:" + aantalLeden + ") moet tussen de 10 en 2500 liggen voor jeugdbeweging " + this.naam);
        } else
        this.aantalLeden = aantalLeden;
    }

    public int getAantalLeiding() {
        return aantalLeiding;
    }

    public void setAantalLeiding(int aantalLeiding) {
        if (aantalLeiding < 10 || aantalLeiding > 500){
            logger.log(Level.SEVERE,"Het aantal leiding (waarde:" + aantalLeiding + ") moet tussen de 10 en 500 liggen voor jeugdbeweging " + this.naam);
        } else
        this.aantalLeiding = aantalLeiding;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public LocalDate getDatumOprichting() {
        return datumOprichting;
    }

    public void setDatumOprichting(LocalDate datumOprichting) {
        if (datumOprichting.isBefore(LocalDate.of(1900,1,1))||datumOprichting.isAfter(LocalDate.now())){
            logger.log(Level.SEVERE,"De oprichtings datum (waarde:" + datumOprichting + ") kan niet voor 1900 of na de datum van vandaag vallen voor jeugdbeweging " + this.naam);
        } else
        this.datumOprichting = datumOprichting;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Jeugdbeweging that = (Jeugdbeweging) o;

        return naam != null ? naam.equals(that.naam) : that.naam == null;
    }

    @Override
    public int hashCode() {
        return naam != null ? naam.hashCode() : 0;
    }

    @Override
    public int compareTo(Jeugdbeweging o) {
        return this.naam.compareTo(o.naam);
    }

    @Override
    public String toString() {
        return String.format("naam: %20s, oppervlakte: %8.2f, aantal leden: %5d, aantal leiding: %5d, type: %10s, datum oprichting: %s",this.naam,this.oppervlakte,this.aantalLeden,this.aantalLeiding,this.type,this.datumOprichting.toString());
    }
}
