import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import be.kdg.JeugdbewegingProject.model.Jeugdbewegingen;
import be.kdg.JeugdbewegingProject.model.Type;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tim Delrue
 * 18/11/2021
 */
class JeugdbewegingenTest {
    Jeugdbeweging j1;
    Jeugdbeweging j2;
    Jeugdbeweging j3;
    Jeugdbeweging j4;
    Jeugdbeweging j5;
    Jeugdbewegingen jeugdbewegingen;

    @BeforeEach
    void setUp() {
        j1 =new Jeugdbeweging("pleplo", 123.4, 75, 14, Type.CHIRO, LocalDate.of(1997, 1, 1));
        j2 = new Jeugdbeweging("21ste scouting L.O.", 160, 85, 16, Type.SCOUTS, LocalDate.of(1985, 5, 1));
        j3 = new Jeugdbeweging("KSA zwijndrecht", 150, 204, 34, Type.KSA, LocalDate.of(1999, 1, 26));
        j4 = new Jeugdbeweging("Scouts zwijndrecht", 76.9, 70, 22, Type.SCOUTS, LocalDate.of(2004, 2, 26));
        j5 = new Jeugdbeweging("Chiro burcht", 124.3, 86, 18, Type.CHIRO, LocalDate.of(2005, 10, 26));
        jeugdbewegingen = new Jeugdbewegingen();

        jeugdbewegingen.add(j1);
        jeugdbewegingen.add(j2);
        jeugdbewegingen.add(j3);
        jeugdbewegingen.add(j4);
        jeugdbewegingen.add(j5);
    }

    @Test
    void testAdd(){
        Jeugdbeweging j1Copy = new Jeugdbeweging(j1.getNaam(),j1.getOppervlakte(),j1.getAantalLeden(),j1.getAantalLeiding(),j1.getType(),j1.getDatumOprichting());
        jeugdbewegingen.add(j1Copy);
        assertTrue(jeugdbewegingen.getSize()<6,"2 keer hetzelfde object toevoegen zou niet moeten kunnen.");
    }

    @Test
    void testRemove() {
        int beginSize = jeugdbewegingen.getSize();
        jeugdbewegingen.remove("pleplo");

        assertTrue(jeugdbewegingen.getSize() == beginSize-1,"Het object zou verwijderd moeten zijn maar dat is niet gebeurd");
        assertFalse(jeugdbewegingen.remove("abcdefghijklmnop"),"Een jeugdbeweging die niet bestaat verwijderen zou 'false' moeten teruggeven maar geeft 'true'");
    }

    @Test
    void testSortAllOnSurface(){
        List<Jeugdbeweging> sortedJeugdbewegingen = jeugdbewegingen.sortedBy(Jeugdbeweging::getOppervlakte);
        assertAll(()-> assertTrue(sortedJeugdbewegingen.get(0).getOppervlakte() == 76.9,"Sorterings fout, dit zou de eerste van de lijst moeten zijn."),
                ()-> assertTrue(sortedJeugdbewegingen.get(1).getOppervlakte() == 123.4, "Sorterings fout, dit zou de tweede van de lijst moeten zijn."),
                () -> assertTrue(sortedJeugdbewegingen.get(2).getOppervlakte() == 124.3, "Sorterings fout, dit zou de derde van de lijst moeten zijn."));
    }

    @Test
    void testSortAllOnAantalLeden(){
        Jeugdbeweging[] sortedArray = {j4,j1,j2,j5,j3};
        assertArrayEquals(sortedArray,jeugdbewegingen.sortedBy(Jeugdbeweging::getAantalLeden).toArray(),"Fout bij sorteren op aantal leden.");
    }
}