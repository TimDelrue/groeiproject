import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import be.kdg.JeugdbewegingProject.model.Type;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tim Delrue
 * 18/11/2021
 */
class JeugdbewegingTest {
    private Jeugdbeweging j1;
    private Jeugdbeweging j2;

    @BeforeEach
    void setUp() {
        j1 = new Jeugdbeweging("Pleplo", 300.5, 78, 12, Type.CHIRO, LocalDate.of(1989, 1, 1));
        j2 = new Jeugdbeweging("21ste scouts linkeroever", 450.1, 98, 14, Type.CHIRO, LocalDate.of(2001, 1, 1));
    }

    @Test
    void testEquals() {
        assertFalse(j1.equals(j2), "De jeugdbewegingen moeten verschillen");
        assertTrue(j1.equals(j1), "De Jeugdbewegingen moeten gelijk zijn");
        Jeugdbeweging j3 = new Jeugdbeweging("Pleplo", 10, 10, 10, Type.SCOUTS, LocalDate.of(1901, 1, 1));
        assertTrue(j1.equals(j3), String.format("De namen van de jeugdbewegingen moet gelijk zijn, waarden: " +
                "Jeugdbeweging 1= %s, Jeugdbeweging2= %s", j1.toString(), j2.toString()));
    }

    @Test
    void testIllegalOppervlakte() {
        assertThrows(IllegalArgumentException.class, () -> j1.setOppervlakte(0), "Een oppervlakte van 0 zou een IllegalArgumentExecption moeten gooien.");
    }

    @Test
    void testLegalAantalLeden() {
        assertDoesNotThrow(()->j1.setAantalLeden(50),"het leden aantal op 50 setten zou geen execption mogen gooien.");
    }

    @Test
    void testCompareTo(){
        assertTrue(j1.compareTo(j2)>1,"'21ste scouts linkeroever' zou voor 'Pleplo' gesorteerd moeten worden");
        Jeugdbeweging j3 = new Jeugdbeweging("Pleplo", 10, 10, 10, Type.SCOUTS, LocalDate.of(1901, 1, 1));
        assertTrue(j1.compareTo(j3)==0,"2 Jeugdbewegingen met dezelfde naam zouden gelijk gesorteerd moeten worden.");
    }

    @Test
    void testOppervlakte(){
        assertEquals(j1.getOppervlakte(),300.5,0.1,"oppervlakte van 300.5 en double waarde van 300.5 moeten gelijk zijn (max 0.1 verschil)");
    }
}