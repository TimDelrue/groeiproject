import be.kdg.JeugdbewegingProject.data.Data;
import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import be.kdg.JeugdbewegingProject.model.Jeugdbewegingen;
import be.kdg.JeugdbewegingProject.model.Type;
import be.kdg.JeugdbewegingProject.util.JeugdbewegingFunctions;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Tim Delrue
 * 21/10/2021
 */
public class Demo_4 {
    public static void main(String[] args) {
        Jeugdbewegingen jeugdbewegingen = new Jeugdbewegingen();
        /*for (Jeugdbeweging jeugdbeweging : Data.getData()){
            jeugdbewegingen.add(jeugdbeweging);
        }*/
        Data.getData().forEach(jeugdbewegingen::add);

        System.out.println("\nJeugdbewegingen gesorteerd op naam:");
        /*for (Jeugdbeweging jeugdbeweging : jeugdbewegingen.sortedBy(Jeugdbeweging::getNaam)) {
            System.out.println(jeugdbeweging);
        }*/
        jeugdbewegingen.sortedBy(Jeugdbeweging::getNaam).forEach(System.out::println);

        System.out.println("\nEnkel jeugdbewegingen met oppervlakte onder de 100m²:");
        List<Jeugdbeweging> list = JeugdbewegingFunctions.filteredList(Data.getData(), j -> j.getOppervlakte()<100);
        list.forEach(System.out::println);

        System.out.println("\nEnkel jeugdbewegingen met meer als 80 leden:");
        JeugdbewegingFunctions.filteredList(Data.getData(), j -> j.getAantalLeden()>80).forEach(System.out::println);

        System.out.println("\nEnkel jeugdbewegingen die een scouts zijn:");
        JeugdbewegingFunctions.filteredList(Data.getData(), j -> j.getType().equals(Type.SCOUTS)).forEach(System.out::println);

        System.out.printf("Gemiddeld oppervlakte jeugdbeweging: %.1f m²\n", JeugdbewegingFunctions.average (Data.getData(), Jeugdbeweging::getOppervlakte));

        System.out.printf("Aantal Jeugdbewegingen met meer als 15 leiding: %d\n",
                JeugdbewegingFunctions.countIf(Data.getData(), j -> j.getAantalLeiding() > 15));

        System.out.printf("Aantal jeugdbewegingen die een chiro zijn: %d\n",
                Data.getData().stream().filter(j -> j.getType().equals(Type.CHIRO)).count());

        System.out.println("Gesorteerd op aantal leden en dan aantal leiding:");
        Data.getData().stream().sorted(Comparator.comparing(Jeugdbeweging::getAantalLeden).thenComparing(Jeugdbeweging::getAantalLeiding))
                .forEach(System.out::println);

        System.out.printf("Alle namen van jeugdbewegingen in hoofdletters, omgekeerd gesorteerd en zonder dubbels: \n%s\n\n",
        Data.getData().stream().distinct().map(Jeugdbeweging::getNaam).map(String::toUpperCase).sorted(Comparator.reverseOrder()).collect(Collectors.joining(", ")));

        System.out.printf("Willekeure jeugdbeweging met meer als 100 leden: \n%s\n\n",
                Data.getData().stream().filter(j1 -> j1.getAantalLeden()>100).findAny().get());

        System.out.printf("De kampioen in oppervlakte is: %s\n",Data.getData().stream().max(Comparator.comparing(Jeugdbeweging::getOppervlakte)).map(Jeugdbeweging::getNaam).get());
        System.out.printf("De kampioen in aantal leiding is: %s\n\n",Data.getData().stream().max(Comparator.comparing(Jeugdbeweging::getAantalLeiding)).map(Jeugdbeweging::getNaam).get());

        System.out.println("List met gesorteerde namen van jeugdbewegingen beginnend met 'Z'");
        System.out.println(Data.getData().stream().map(Jeugdbeweging::getNaam).filter(j1 -> j1.startsWith("Z")).sorted().collect(Collectors.toList()));

        System.out.println("Jeugdbewegingen ontstaan voor 2000");
        Data.getData().stream().filter(j1 -> j1.getDatumOprichting().isBefore(LocalDate.of(2000,1,1))).forEach(System.out::println);
        System.out.println("Jeugdbewegingen ontstaan na 2000");
        Data.getData().stream().filter(j1 -> j1.getDatumOprichting().isAfter(LocalDate.of(2000,1,1))).forEach(System.out::println);

        System.out.println("Alle jeugdbewegingen per soort");
        Data.getData().stream().collect(Collectors.groupingBy(Jeugdbeweging::getType)).forEach((k1, j1) -> System.out.printf("%20s, %s\n",k1,j1.stream().map(Jeugdbeweging::getNaam).collect(Collectors.joining(", "))));
    }

    // not 4.Steams en lambda's doen, rest is al klaar
}
