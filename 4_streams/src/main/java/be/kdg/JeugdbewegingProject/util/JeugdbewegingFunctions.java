package be.kdg.JeugdbewegingProject.util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;

/**
 * Tim Delrue
 * 21/10/2021
 */
public class JeugdbewegingFunctions {
    public static <T> List<T> filteredList(List<T> jeugdbewegingList, Predicate<T> predicate) {
        List<T> result = new ArrayList<>();
        /*for (T t : jeugdbewegingList)
        {
            if (predicate.test(t)) {
                result.add(t);
            }
        }*/
        jeugdbewegingList.stream().filter(predicate).forEach(result::add);
        return result;
    }

    public static <T> Double average (List<T> jeugdbewegingList, ToDoubleFunction<T> mapper){
        double total = 0;
        /*for (T t : jeugdbewegingList){
            total += mapper.applyAsDouble(t);
        }*/
        total += jeugdbewegingList.stream().mapToDouble(mapper).sum();
        return total/jeugdbewegingList.size();
    }

    public static <T> long countIf(List<T> jeugdbewegingList, Predicate<T> predicate){
        /*Long counter = 0L;
        for (T t : jeugdbewegingList)
        {
            if (predicate.test(t)) {
                counter++;
            }
        }*/
        return jeugdbewegingList.stream().filter(predicate).count();
    }
}
