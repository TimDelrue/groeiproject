package be.kdg.JeugdbewegingProject.exceptions;

/**
 * Tim Delrue
 * 23/01/2022
 */
public class JeugdbewegingException extends RuntimeException{
    public JeugdbewegingException(Throwable cause) {
        super(cause);
    }

}
