package be.kdg.JeugdbewegingProject.service;

import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;

import java.util.List;

/**
 * Tim Delrue
 * 23/01/2022
 */
public interface JeugdbewegingenService {
    public List<Jeugdbeweging> getAllJeugdbewegingen();
    public void addJeugdbeweging(Jeugdbeweging jeugdbeweging);
}
