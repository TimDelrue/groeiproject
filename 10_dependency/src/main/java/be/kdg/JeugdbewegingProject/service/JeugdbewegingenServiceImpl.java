package be.kdg.JeugdbewegingProject.service;

import be.kdg.JeugdbewegingProject.database.JeugdbewegingDao;
import be.kdg.JeugdbewegingProject.database.JeugdbewegingDbDao;
import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;

import java.util.List;

/**
 * Tim Delrue
 * 23/01/2022
 */
public class JeugdbewegingenServiceImpl implements JeugdbewegingenService{
    JeugdbewegingDao jeugdbewegingDao = JeugdbewegingDbDao.getInstance();

    @Override
    public List<Jeugdbeweging> getAllJeugdbewegingen() {
        return jeugdbewegingDao.getAllJeugdbewegingen();
    }

    @Override
    public void addJeugdbeweging(Jeugdbeweging jeugdbeweging) {
        jeugdbewegingDao.insert(jeugdbeweging);
    }
}
