package be.kdg.JeugdbewegingProject;

import be.kdg.JeugdbewegingProject.database.JeugdbewegingDao;
import be.kdg.JeugdbewegingProject.database.JeugdbewegingDbDao;
import be.kdg.JeugdbewegingProject.service.JeugdbewegingenService;
import be.kdg.JeugdbewegingProject.service.JeugdbewegingenServiceImpl;
import be.kdg.JeugdbewegingProject.view.JeugdbewegingenPresenter;
import be.kdg.JeugdbewegingProject.view.JeugdbewegingenView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Tim Delrue
 * 23/01/2022
 */
public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        JeugdbewegingDao jeugdbewegingDao = JeugdbewegingDbDao.getInstance();
        JeugdbewegingenService jeugdbewegingenService = new JeugdbewegingenServiceImpl();
        JeugdbewegingenView jeugdbewegingenView = new JeugdbewegingenView();
        JeugdbewegingenPresenter jeugdbewegingenPresenter = new JeugdbewegingenPresenter(jeugdbewegingenService,jeugdbewegingenView);

        primaryStage.setScene(new Scene(jeugdbewegingenView));
        primaryStage.show();
    }
}
