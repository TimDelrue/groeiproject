package be.kdg.JeugdbewegingProject.view;

import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import be.kdg.JeugdbewegingProject.model.Type;
import be.kdg.JeugdbewegingProject.service.JeugdbewegingenService;
import javafx.collections.FXCollections;
import javafx.scene.control.Alert;

import java.lang.reflect.Executable;

/**
 * Tim Delrue
 * 23/01/2022
 */
public class JeugdbewegingenPresenter {
    private JeugdbewegingenService jeugdbewegingenService;
    private JeugdbewegingenView jeugdbewegingenView;

    public JeugdbewegingenPresenter(JeugdbewegingenService jeugdbewegingenService, JeugdbewegingenView jeugdbewegingenView) {
        this.jeugdbewegingenService = jeugdbewegingenService;
        this.jeugdbewegingenView = jeugdbewegingenView;
        showJeugdbewegingen();
        addEventHandlers();
    }

    private void addEventHandlers(){
        jeugdbewegingenView.getButton().setOnAction(event -> {
            try{
                Jeugdbeweging jeugdbeweging = new Jeugdbeweging(
                        jeugdbewegingenView.getTextFieldNaam().getText(),
                        100,
                        Integer.parseInt(jeugdbewegingenView.getTextFieldAantalLeden().getText()),
                        15,
                        Type.CHIRO,
                        jeugdbewegingenView.getDatePicker().getValue()
                );
                jeugdbewegingenService.addJeugdbeweging(jeugdbeweging);
            } catch (Exception e){
                Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
                alert.showAndWait();
            }
            showJeugdbewegingen();
        });
    }

    private void showJeugdbewegingen(){
        try{
            jeugdbewegingenView.getTableView().setItems(FXCollections.observableList(jeugdbewegingenService.getAllJeugdbewegingen()));
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
            alert.showAndWait();
        }
    }
}
