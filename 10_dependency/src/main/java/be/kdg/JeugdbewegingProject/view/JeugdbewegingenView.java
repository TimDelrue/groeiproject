package be.kdg.JeugdbewegingProject.view;

import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

import java.time.LocalDate;

/**
 * Tim Delrue
 * 23/01/2022
 */
public class JeugdbewegingenView extends BorderPane {
    private TableView tableView;
    private TextField textFieldNaam;
    private TextField textFieldAantalLeden;
    private DatePicker datePicker;

    private Button button;

    public JeugdbewegingenView() {
        tableView = new TableView();
        TableColumn<Jeugdbeweging, String> column1 = new TableColumn<Jeugdbeweging, String>("Naam");
        column1.setCellValueFactory(new PropertyValueFactory<>("naam"));
        TableColumn<Jeugdbeweging, Integer> column2 = new TableColumn<Jeugdbeweging, Integer>("aantal leden");
        column2.setCellValueFactory(new PropertyValueFactory<>("aantalLeden"));
        TableColumn<Jeugdbeweging, LocalDate> column3 = new TableColumn<Jeugdbeweging, LocalDate>("datum oprichting");
        column3.setCellValueFactory(new PropertyValueFactory<>("datumOprichting"));

        tableView.getColumns().add(column1);
        tableView.getColumns().add(column2);
        tableView.getColumns().add(column3);

        setCenter(tableView);

        textFieldNaam = new TextField();
        textFieldAantalLeden = new TextField();
        datePicker = new DatePicker();

        textFieldNaam.setPromptText("Naam");
        textFieldAantalLeden.setPromptText("Aantal leden");
        datePicker.setPromptText("Datum oprichting");

        button = new Button("Save");

        HBox hBox = new HBox(textFieldNaam,textFieldAantalLeden,datePicker,button);
        this.setBottom(hBox);
    }

    TableView getTableView() {
        return tableView;
    }

    TextField getTextFieldNaam() {
        return textFieldNaam;
    }

    TextField getTextFieldAantalLeden() {
        return textFieldAantalLeden;
    }

    DatePicker getDatePicker() {
        return datePicker;
    }

    Button getButton() {
        return button;
    }
}
