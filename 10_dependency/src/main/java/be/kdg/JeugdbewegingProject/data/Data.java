package be.kdg.JeugdbewegingProject.data;

import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import be.kdg.JeugdbewegingProject.model.Type;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Tim Delrue
 * 05/10/2021
 */
public class Data {
    public static ArrayList<Jeugdbeweging> getData(){
        ArrayList<Jeugdbeweging> result = new ArrayList<>();

        result.add(new Jeugdbeweging("pleplo", 123.4,75,14, Type.CHIRO, LocalDate.of(1997,1,1)));
        result.add(new Jeugdbeweging("21ste scouting L.O.", 160,85,16, Type.SCOUTS,LocalDate.of(1985,5,1)));
        result.add(new Jeugdbeweging("KSA zwijndrecht", 150,204,34, Type.KSA,LocalDate.of(1999,1,26)));
        result.add(new Jeugdbeweging("Scouts zwijndrecht", 76.9,70,22, Type.SCOUTS,LocalDate.of(2004,2,26)));
        result.add(new Jeugdbeweging("Chiro burcht", 124.3,86,18, Type.CHIRO,LocalDate.of(2005,10,26)));

        result.add(new Jeugdbeweging("Scouts 9 en 10", 99.5,65,19, Type.SCOUTS,LocalDate.of(1995,9,6)));
        result.add(new Jeugdbeweging("Zeescouts L.O. 1", 106.3,106,25, Type.ZEESCOUTS,LocalDate.of(1985,9,6)));
        result.add(new Jeugdbeweging("Zeescouts L.O. 2", 94.6,112,24, Type.ZEESCOUTS,LocalDate.of(1986,11,5)));
        result.add(new Jeugdbeweging("Zeescouts L.O. 3", 154.56,101,17, Type.ZEESCOUTS,LocalDate.of(2002,5,6)));
        result.add(new Jeugdbeweging("Zeescouts L.O. 4", 90.2,68,12, Type.ZEESCOUTS,LocalDate.of(2006,7,6)));

        result.add(new Jeugdbeweging("Scouts zuid", 97.123,93,15, Type.SCOUTS,LocalDate.of(1998,7,6)));
        result.add(new Jeugdbeweging("KLJ Beveren", 120,80,16, Type.KLJ,LocalDate.of(2004,7,4)));
        result.add(new Jeugdbeweging("Scouts Beveren", 105.63,96,21, Type.SCOUTS,LocalDate.of(1988,5,3)));
        result.add(new Jeugdbeweging("KLJ Melsele", 101.11,100,22, Type.KLJ,LocalDate.of(1984,5,3)));
        result.add(new Jeugdbeweging("Scouts Burcht", 124,77,18, Type.SCOUTS,LocalDate.of(1993,9,7)));
        return result;
    }
}
