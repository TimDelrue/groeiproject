package be.kdg.JeugdbewegingProject;


import be.kdg.JeugdbewegingProject.kollections.lists.ArrayList;
import be.kdg.JeugdbewegingProject.kollections.Kollections;
import be.kdg.JeugdbewegingProject.kollections.lists.LinkedList;
import be.kdg.JeugdbewegingProject.kollections.lists.List;
import be.kdg.JeugdbewegingProject.kollections.maps.HashMap;
import be.kdg.JeugdbewegingProject.kollections.maps.ListMap;
import be.kdg.JeugdbewegingProject.kollections.maps.Map;
import be.kdg.JeugdbewegingProject.kollections.sets.ArraySet;
import be.kdg.JeugdbewegingProject.kollections.sets.Set;
import be.kdg.JeugdbewegingProject.kollections.sets.TreeSet;
import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import be.kdg.JeugdbewegingProject.model.JeugdbewegingFactory;

public class PerformanceTester {

    public static List<Jeugdbeweging> randomList(int n) {
        List<Jeugdbeweging> myList = new LinkedList<>();
        for (int i = 0; i < n; i++) {
            myList.add(JeugdbewegingFactory.newRandomJeugdbeweging());
        }
        return myList;
    }

    public static List<Jeugdbeweging> emptyList(int n) {
        List<Jeugdbeweging> myList = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            myList.add(JeugdbewegingFactory.newEmptyJeugdbeweging());
        }
        return myList;
    }

    public static void compareArrayListAndLinkedList(int n) {
        Long timeStartTest = System.currentTimeMillis();
        List<Jeugdbeweging> arrayList = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            arrayList.add(0, JeugdbewegingFactory.newRandomJeugdbeweging());
        }
        Long timeArrayListAdding = System.currentTimeMillis() - timeStartTest;
        timeStartTest = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            arrayList.get(arrayList.size() - 1);
        }
        Long timeArrayListGetting = System.currentTimeMillis() - timeStartTest;
        timeStartTest = System.currentTimeMillis();
        List<Jeugdbeweging> linkedList = new LinkedList<>();
        for (int i = 0; i < n; i++) {
            linkedList.add(0, JeugdbewegingFactory.newRandomJeugdbeweging());
        }
        Long timeLinkedListAdding = System.currentTimeMillis() - timeStartTest;
        timeStartTest = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            linkedList.get(linkedList.size() - 1);
        }
        Long timeLinkedListGetting = System.currentTimeMillis() - timeStartTest;
        System.out.println("Time Arraylist adding " + n + " (in milliseconds):  " + timeArrayListAdding);
        System.out.println("Time Linkedlist adding " + n + " (in milliseconds): " + timeLinkedListAdding);
        System.out.println("time getting " + n + " from arraylist:              " + timeArrayListGetting);
        System.out.println("time getting " + n + " from Linkedlist:             " + timeLinkedListGetting);
    }

    public static void testSelectionSort() {
        for (int n=1000;n<20000;n+=1000){
            Kollections.selectionSort(emptyList(n));
            System.out.println(n + ";" + Jeugdbeweging.compareCounter);
            Jeugdbeweging.compareCounter = 0;
        }
    }

    public static void testMergeSort() {
        for (int n=1000;n<20000;n+=1000){
            Kollections.mergeSort(emptyList(n));
            System.out.println(n + ";" + Jeugdbeweging.compareCounter);
            Jeugdbeweging.compareCounter = 0;
        }
    }

    public static void compareListMapToHasMap(int size){
        ListMap<String, Jeugdbeweging> listMap = new ListMap<>();
        HashMap<String, Jeugdbeweging> hashMap = new HashMap<>();

        fillMap(listMap,size);
        fillMap(hashMap,size);

        Jeugdbeweging.equalsCounter = 0;
        long startTime = System.nanoTime();

        for (int i = 0; i < size; i++) {
            listMap.get("Jeugdbeweging " + i);
        }
        System.out.printf("Listmap: n = %5d, equalscount = %10d, nanasec = %10d\n",size,Jeugdbeweging.equalsCounter,System.nanoTime() - startTime);

        Jeugdbeweging.equalsCounter = 0;

        startTime = System.nanoTime();
        for (int i = 0; i < size; i++) {
            hashMap.get("Jeugdbeweging " + i);
        }
        System.out.printf("Hashmap: n = %5d, equalscount = %10d, nanasec = %10d\n",size,Jeugdbeweging.equalsCounter,System.nanoTime() - startTime);
    }

    private static void fillMap(Map<String, Jeugdbeweging> map, int n){
        for (int i = 0; i < n; i++) {
            Jeugdbeweging smartphone = JeugdbewegingFactory.newRandomJeugdbeweging();
            smartphone.setNaam("Jeugdbeweging " + i);
            map.put(smartphone.getNaam(), smartphone);
        }
    }

    public static void compareArraySetToTreeSet(){
        int size = 1000;
        ArraySet<Jeugdbeweging> arraySet = new ArraySet<>();
        TreeSet<Jeugdbeweging> treeSet = new TreeSet<>();

        Jeugdbeweging.compareCounter = 0;
        Jeugdbeweging.equalsCounter = 0;

        long startTime = System.nanoTime();

        fillSet(arraySet,size);
        System.out.printf("Arrayset, n = %5d: equalscount: %d\n",size,Jeugdbeweging.equalsCounter);
        System.out.printf("Arrayset, n = %5d: comparecount: %d\n",size,Jeugdbeweging.compareCounter);
        System.out.printf("Arrayset, n = %5d: nanosec: %d\n",size,System.nanoTime() - startTime);

        Jeugdbeweging.compareCounter = 0;
        Jeugdbeweging.equalsCounter = 0;

        startTime = System.nanoTime();

        fillSet(treeSet,size);
        System.out.printf("Treeset, n = %5d: equalscount: %d\n",size,Jeugdbeweging.equalsCounter);
        System.out.printf("Treeset, n = %5d: comparecount: %d\n",size,Jeugdbeweging.compareCounter);
        System.out.printf("Treeset, n = %5d: nanosec: %d\n",size,System.nanoTime() - startTime);
    }

    private static void fillSet(Set<Jeugdbeweging> set, int n){
        for(int i = 0; i < n; i++){
            Jeugdbeweging smartphone = JeugdbewegingFactory.newRandomJeugdbeweging();
            set.add(smartphone);
        }
    }
}
