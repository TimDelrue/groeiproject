package be.kdg.JeugdbewegingProject.model;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Tim Delrue
 * 05/10/2021
 */
public class Jeugdbeweging implements Comparable<Jeugdbeweging> {
    private String naam;
    private double oppervlakte;
    private int aantalLeden;
    private int aantalLeiding;
    private Type type;
    private LocalDate datumOprichting;

    public static int compareCounter = 0;
    public static int equalsCounter = 0;

    protected Jeugdbeweging() {
        this.naam = "Onbekend";
        this.oppervlakte = 100;
        this.aantalLeden = 100;
        this.aantalLeiding = 10;
        this.type = Type.SCOUTS;
        this.datumOprichting = LocalDate.of(2000,1,1);
    }

    protected Jeugdbeweging(String naam, double oppervlakte, int aantalLeden, int aantalLeiding, Type type, LocalDate datumOprichting) {
        this.setNaam(naam);
        this.setOppervlakte(oppervlakte);
        this.setAantalLeden(aantalLeden);
        this.setAantalLeiding(aantalLeiding);
        this.setType(type);
        this.setDatumOprichting(datumOprichting);
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public double getOppervlakte() {
        return oppervlakte;
    }

    public void setOppervlakte(double oppervlakte) throws IllegalArgumentException{
        if (oppervlakte < 10 || oppervlakte > 10000){
            throw new IllegalArgumentException("De oppervlakte moet tussen 10 en 10000 M² liggen.");
        }
        this.oppervlakte = oppervlakte;
    }

    public int getAantalLeden() {
        return aantalLeden;
    }

    public void setAantalLeden(int aantalLeden) {
        if (aantalLeden < 10 || aantalLeden > 2500){
            throw new IllegalArgumentException("Het aantal leden moet tussen de 10 en 2500 liggen");
        }
        this.aantalLeden = aantalLeden;
    }

    public int getAantalLeiding() {
        return aantalLeiding;
    }

    public void setAantalLeiding(int aantalLeiding) {
        if (aantalLeiding < 10 || aantalLeiding > 500){
            throw new IllegalArgumentException("Het aantal leiding moet tussen de 10 en 500 liggen");
        }
        this.aantalLeiding = aantalLeiding;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public LocalDate getDatumOprichting() {
        return datumOprichting;
    }

    public void setDatumOprichting(LocalDate datumOprichting) {
        if (datumOprichting.isBefore(LocalDate.of(1900,1,1))||datumOprichting.isAfter(LocalDate.now())){
            throw new IllegalArgumentException("De oprichtings datum kan niet voor 1900 of na de datum van vandaag vallen.");
        }
        this.datumOprichting = datumOprichting;
    }

    @Override
    public boolean equals(Object o) {
        equalsCounter++;
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Jeugdbeweging that = (Jeugdbeweging) o;

        return Objects.equals(naam, that.naam);
    }

    @Override
    public int hashCode() {
        return naam != null ? naam.hashCode() : 0;
    }

    @Override
    public int compareTo(Jeugdbeweging o) {
        compareCounter++;
        return this.naam.compareTo(o.naam);
    }

    @Override
    public String toString() {
        return String.format("naam: %20s, oppervlakte: %8.2f, aantal leden: %5d, aantal leiding: %5d, type: %10s, datum oprichting: %s",this.naam,this.oppervlakte,this.aantalLeden,this.aantalLeiding,this.type,this.datumOprichting.toString());
    }
}
