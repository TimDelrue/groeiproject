package be.kdg.JeugdbewegingProject.kollections.lists;


import be.kdg.JeugdbewegingProject.kollections.Kollections;
import be.kdg.JeugdbewegingProject.kollections.lists.List;

public class LinkedList<E> implements List<E> {
    private Node<E> root;
    private int size;
    public LinkedList() {
    }

    @Override
    public void add(int index, E element) {
        if (index > this.size || index < 0) {
            throw new IndexOutOfBoundsException("index: " + index + ", size: " + size);
        }
        Node<E> node = root;

        if (root == null) {
            root = new Node<>(element);
        } else {
            for (int i = 0; i < index-1; i++) {
                node = node.next;
            }
            if (index == 0){
                Node<E> newRoot = new Node<>(element);
                newRoot.next = root;
                root = newRoot;
            } else {
                Node<E> newNode = new Node<>(element);
                newNode.next = node.next;
                node.next = newNode;
            }
        }
        size++;
    }

    @Override
    public void add(E element) {
        add(size, element);
    }

    @Override
    public boolean remove(E element) {
        if (this.contains(element)){
            remove(Kollections.lineairSearch(this,element));
        }
        return false;
    }

    @Override
    public boolean contains(E element) {
        if (Kollections.lineairSearch(this, element) == -1){
            return false;
        }
        return true;
    }

    @Override
    public void set(int index, E element) {
        if (index > this.size || index < 0) {
            throw new IndexOutOfBoundsException("index: " + index + ", size: " + size);
        }
        Node<E> node = root;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        node.value = element;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public E remove(int index) {
        if (index >= this.size || index < 0) {
            throw new IndexOutOfBoundsException("index: " + index + ", size: " + size);
        }
        if (index == 0) {
            E oldElement = root.value;
            root = root.next;
            size--;
            return oldElement;
        } else {
            Node<E> beforeNode = root;
            for (int i = 1; i < index; i++) {
                beforeNode = beforeNode.next;
            }
            E oldElement = beforeNode.next.value;
            beforeNode.next = beforeNode.next.next;
            size--;
            return oldElement;
        }
    }

    @Override
    public int indexOf(E element) {
        return Kollections.lineairSearch(this, element);
    }

    @Override
    public E get(int index) {
        if (index >= this.size || index < 0) {
            throw new IndexOutOfBoundsException("index: " + index + ", size: " + size);
        }
        Node<E> node = root;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node.value;
    }

    static class Node<V> {
        V value;
        Node<V> next;

        public Node(V value) {
            this.value = value;
        }
    }
}
