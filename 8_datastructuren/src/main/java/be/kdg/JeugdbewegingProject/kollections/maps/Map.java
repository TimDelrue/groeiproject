package be.kdg.JeugdbewegingProject.kollections.maps;

import be.kdg.JeugdbewegingProject.kollections.lists.List;
import be.kdg.JeugdbewegingProject.kollections.sets.Set;

/**
 * Tim Delrue
 * 21/01/2022
 */
public interface Map<K, V> {
    void put(K key, V value);
    V get(K key);
    List<V> values();
    Set<K> keySet();
    int size();
}
