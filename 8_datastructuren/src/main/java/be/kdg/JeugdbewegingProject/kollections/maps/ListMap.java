package be.kdg.JeugdbewegingProject.kollections.maps;

import be.kdg.JeugdbewegingProject.kollections.lists.ArrayList;
import be.kdg.JeugdbewegingProject.kollections.lists.List;
import be.kdg.JeugdbewegingProject.kollections.sets.ArraySet;
import be.kdg.JeugdbewegingProject.kollections.sets.Set;

/**
 * Tim Delrue
 * 21/01/2022
 */
public class ListMap<K,V> implements Map<K, V> {
    private List<K> keys = new ArrayList<>();
    private List<V> values = new ArrayList<>();
    private int size;

    @Override
    public void put(K key, V value) {
        int index = keys.indexOf(key);
        if (index!=-1) {
            values.set(index, value);
        } else {
            keys.add(key);
            values.add(value);
            size++;
        }
    }

    @Override
    public V get(K key) {
        int index = keys.indexOf(key);
        if (index!=-1) {
            return values.get(index);
        }
        return null;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public List<V> values() {
        return values;
    }

    @Override
    public Set<K> keySet() {
        ArraySet<K> keySet = new ArraySet<>();
        for (int i=0;i<keys.size();i++) {
            keySet.add(keys.get(i));
        }
        return keySet;
    }
}

