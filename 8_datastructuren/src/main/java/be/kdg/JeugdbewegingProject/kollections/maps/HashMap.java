package be.kdg.JeugdbewegingProject.kollections.maps;

import be.kdg.JeugdbewegingProject.kollections.lists.ArrayList;
import be.kdg.JeugdbewegingProject.kollections.lists.List;
import be.kdg.JeugdbewegingProject.kollections.sets.ArraySet;
import be.kdg.JeugdbewegingProject.kollections.sets.Set;

/**
 * Tim Delrue
 * 21/01/2022
 */
public class HashMap<K, V> implements Map<K, V> {
    private static final int DEFAULT_CAPACITY = 100;
    private Node<K, V>[] buckets;
    private int size = 0;
    public HashMap() {
        this(DEFAULT_CAPACITY);
    }

    public HashMap(int capacity) {
        buckets = new Node[capacity];
    }

    private int hash(K key) {
        return Math.abs(key.hashCode() % buckets.length);
    }

    @Override
    public void put(K key, V value) {
        int hashKey = hash(key);
        Node<K, V> tmpBucket = buckets[hashKey];
        if (tmpBucket != null) {
            while (tmpBucket.next != null) {
                tmpBucket = tmpBucket.next;
            }
            tmpBucket.next = new Node<>(key, value);
        } else {
            buckets[hashKey] = new Node<>(key, value);
        }
        size++;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public V get(K key) {
        int currentIndex = hash(key);

        Node<K, V> currentNode = buckets[currentIndex];
        if (currentNode == null) {
            return null;
        }

        if (key == currentNode.key) {
            return currentNode.value;
        } else {
            while (currentNode.next != null) {
                currentNode = currentNode.next;
                if (key == currentNode.key) {
                    return currentNode.value;
                }
            }
        }
        return null;
    }

    @Override
    public List<V> values() {
        List<V> values = new ArrayList<>(size);
        for (Node<K, V> bucket : buckets) {
            Node<K, V> node = bucket;
            while (node != null) {
                values.add(node.value);
                node = node.next;
            }
        }
        return values;
    }

    @Override
    public Set<K> keySet() {
        Set<K> keySet = new ArraySet<>();
        for (Node<K, V> bucket : buckets) {
            Node<K, V> node = bucket;
            while (node != null) {
                keySet.add(node.key);
                node = node.next;
            }
        }
        return keySet;
    }

    static class Node<K, V> {
        K key;
        V value;
        Node<K, V> next;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
}

