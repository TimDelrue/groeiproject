package be.kdg.JeugdbewegingProject.kollections.sets;

import be.kdg.JeugdbewegingProject.kollections.Collection;
import be.kdg.JeugdbewegingProject.kollections.lists.List;

/**
 * Tim Delrue
 * 21/01/2022
 */
public interface Set<E> extends Collection<E> {
    List<E> toList();
}