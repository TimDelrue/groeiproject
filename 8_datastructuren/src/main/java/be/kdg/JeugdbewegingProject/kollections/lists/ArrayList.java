package be.kdg.JeugdbewegingProject.kollections.lists;

import be.kdg.JeugdbewegingProject.kollections.Kollections;
import be.kdg.JeugdbewegingProject.kollections.lists.List;

public class ArrayList<E> implements List<E> {
    private static final int INITIAL_CAPACITY = 10;
    private Object[] elements;
    private int size;

    public ArrayList() {
        elements = new Object[INITIAL_CAPACITY];
        size = 0;
    }

    public ArrayList(int size) {
        elements = new Object[size];
        this.size = size;
    }

    private void expand() {
        Object[] newElements = new Object[size + 1];
        /*for (int i = 0; i < size; i++) {
            newElements[i] = elements[i];
        }*/
        System.arraycopy(elements,0,newElements,0,size);
        size++;
        elements = newElements;
    }

    @Override
    public void add(int index, E element) {
        if (index > this.size || index < 0) {
            throw new IndexOutOfBoundsException("index: " + index + ", size: " + size);
        }
        expand();
        for (int i = 0; i < size - index -1; i++) {
            elements[size - i -1] = elements[size - i - 2];
        }
        elements[index] = element;
    }

    @Override
    public void add(E element) {
        add(size, element);
    }

    @Override
    public boolean remove(E element) {
        if (this.contains(element)){
            remove(Kollections.lineairSearch(this,element));
        }
        return false;
    }

    @Override
    public boolean contains(E element) {
        if (Kollections.lineairSearch(this, element) == -1){
            return false;
        }
        return true;
    }

    @Override
    public void set(int index, E element) {
        if (index > this.size || index < 0) {
            throw new IndexOutOfBoundsException("index: " + index + ", size: " + size);
        }
        elements[index] = element;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    @SuppressWarnings("unchecked")
    public E remove(int index) {
        if (index >= this.size || index < 0) {
            throw new IndexOutOfBoundsException("index: " + index + ", size: " + size);
        }
        E oldValue = (E) elements[index];
        for (int i = index; i < size - 1; i++) {
            elements[i] = elements[i + 1];
        }
        size--;
        return oldValue;
    }

    @Override
    public int indexOf(E element) {
        return Kollections.lineairSearch(this, element);
    }

    @Override
    @SuppressWarnings("unchecked")
    public E get(int index) {
        if (index >= this.size || index < 0) {
            throw new IndexOutOfBoundsException("index: " + index + ", size: " + size);
        }
        return (E) elements[index];
    }
}
