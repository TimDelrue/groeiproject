package be.kdg.JeugdbewegingProject;

import be.kdg.JeugdbewegingProject.kollections.lists.ArrayList;
import be.kdg.JeugdbewegingProject.kollections.Kollections;
import be.kdg.JeugdbewegingProject.kollections.lists.List;
import be.kdg.JeugdbewegingProject.model.JeugdbewegingFactory;
import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import be.kdg.JeugdbewegingProject.model.Type;

import java.time.LocalDate;
import java.util.stream.Stream;

/**
 * Tim Delrue
 * 25/11/2021
 */
public class Demo_8 {
    public static void main(String[] args) {
        System.out.println("Empty jeugdbeweging:\n" + JeugdbewegingFactory.newEmptyJeugdbeweging());
        System.out.println("\n\n Filled jeugdbeweging:\n" + JeugdbewegingFactory.newFilledJeugdbeweging("test",100.11,50,12, Type.SCOUTS, LocalDate.of(2001,2,3)));
        System.out.println("\n\n 30 random jeugdbewegingen gesorteerd:");
        Stream.generate(JeugdbewegingFactory::newRandomJeugdbeweging).limit(30).sorted().forEach(System.out::println);
        System.out.println("\nEigen List: \n");
        List<Jeugdbeweging> myList = PerformanceTester.randomList(20);
        for (int i = 0; i < 20; i++) {
            System.out.println(myList.get(i));
        }
        PerformanceTester.compareArrayListAndLinkedList(5000);

        List<Jeugdbeweging> listSelectionSort = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            listSelectionSort.add(JeugdbewegingFactory.newRandomJeugdbeweging());
        }
        Kollections.selectionSort(listSelectionSort);
        System.out.println("Sorted list (Kollections selectionSort): \n");
        for (int i = 0; i < listSelectionSort.size(); i++) {
            System.out.println(listSelectionSort.get(i));
        }

        List<Jeugdbeweging> listMergeSort = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            listMergeSort.add(JeugdbewegingFactory.newRandomJeugdbeweging());
        }
        Kollections.mergeSort(listMergeSort);
        System.out.println("Sorted list (Kollections mergesort): \n");
        for (int i = 0; i < listMergeSort.size(); i++) {
            System.out.println(listMergeSort.get(i));
        }

        System.out.println("Test selection sort:");
        PerformanceTester.testSelectionSort();
        System.out.println("Test merge sort:");
        PerformanceTester.testMergeSort();

        System.out.println("Quick sort:");
        List<Jeugdbeweging> quickSortList = PerformanceTester.randomList(30);
        Kollections.quickSort(quickSortList);
        for (int i = 0; i < 30; i++) {
            System.out.println(quickSortList.get(i));
        }

        Jeugdbeweging toSearchForJeugdbeweging = quickSortList.get(10);
        System.out.println("(lineair search) index of " + toSearchForJeugdbeweging.getNaam() + ": " + Kollections.lineairSearch(quickSortList,toSearchForJeugdbeweging));
        System.out.println("(binary search)  index of " + toSearchForJeugdbeweging.getNaam() + ": " + Kollections.binarySearch(quickSortList,toSearchForJeugdbeweging));
        System.out.println("(binary search)  index of " + JeugdbewegingFactory.newEmptyJeugdbeweging().getNaam() + ": " + Kollections.lineairSearch(quickSortList,JeugdbewegingFactory.newEmptyJeugdbeweging()));
        System.out.println("(binary search)  index of " + JeugdbewegingFactory.newEmptyJeugdbeweging().getNaam() + ": " + Kollections.binarySearch(quickSortList,JeugdbewegingFactory.newEmptyJeugdbeweging()));

        PerformanceTester.compareListMapToHasMap(1000);

        PerformanceTester.compareArraySetToTreeSet();
    }
}
