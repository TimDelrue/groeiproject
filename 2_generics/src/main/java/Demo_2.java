import be.kdg.JeugdbewegingProject.data.Data;
import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;

import java.util.Random;

import be.kdg.JeugdbewegingProject.Generics.PriorityQueue;

/**
 * Tim Delrue
 * 05/10/2021
 */
public class Demo_2 {
    public static void main(String[] args) {
        /*Jeugdbewegingen jeugdbewegingen = new Jeugdbewegingen();
        for (Jeugdbeweging jeugdbeweging : Data.getData()){
            jeugdbewegingen.add(jeugdbeweging);
        }
        Jeugdbeweging dubbel = new Jeugdbeweging("pleplo", 123.4,75,14, Type.CHIRO, LocalDate.of(1997,1,1));
        jeugdbewegingen.add(dubbel);

        jeugdbewegingen.search("pleplo");
        jeugdbewegingen.remove("pleplo");
        System.out.println(jeugdbewegingen.getSize());

        System.out.println("Sorteren op naam:");
        for (Jeugdbeweging jeugdbeweging : jeugdbewegingen.sortedOnName()){
            System.out.println(jeugdbeweging.toString());
        }
        System.out.println("\nSorteren op oppervlakte:");
        for (Jeugdbeweging jeugdbeweging : jeugdbewegingen.sortedOnSurface()){
            System.out.println(jeugdbeweging.toString());
        }
        System.out.println("\nSorteren op aantal leden:");
        for (Jeugdbeweging jeugdbeweging : jeugdbewegingen.sortedOnMemberCount()){
            System.out.println(jeugdbeweging.toString());
        }*/

        /*PriorityQueue<String> myQueue = new PriorityQueue<>();
        myQueue.enqueue("Tokio", 2);
        myQueue.enqueue("Denver", 5);
        myQueue.enqueue("Rio", 2);
        myQueue.enqueue("Oslo", 3);
        System.out.println("Overzicht van de PriorityQueue:");
        System.out.println(myQueue.toString());
        System.out.println("aantal: " + myQueue.getSize());
        System.out.println("positie van Tokio: " + myQueue.search("Tokio"));
        System.out.println("positie van Nairobi: " + myQueue.search("Nairobi"));
        for(int i = 0; i < 4; i++) {System.out.println("Dequeue: " + myQueue.dequeue());
        }System.out.println("Size na dequeue: " + myQueue.getSize());*/

        PriorityQueue<Jeugdbeweging> jeugdQueue = new PriorityQueue<>();
        Random random = new Random();
        for (Jeugdbeweging j : Data.getData()){
            System.out.printf("Enqueue %s: \n%b\n",j.toString(),jeugdQueue.enqueue(j, random.nextInt(4)+1));
        }

        System.out.printf("\n\nQueue:\n%s\n\n",jeugdQueue.toString());

        int size = jeugdQueue.getSize();
        for (int i = 0; i < size; i++) {
            System.out.printf("Dequeue %d : %s\n",i,jeugdQueue.dequeue().toString());
        }
    }
}
