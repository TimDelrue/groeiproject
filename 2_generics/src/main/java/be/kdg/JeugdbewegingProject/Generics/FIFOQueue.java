package be.kdg.JeugdbewegingProject.Generics;

/**
 * Tim Delrue
 * 06/10/2021
 */
public interface FIFOQueue<T> {
    boolean enqueue(T element, int priority);

    T dequeue();

    int search(T element);

    int getSize();
}
