package be.kdg.JeugdbewegingProject.Generics;

import java.util.*;

/**
 * Tim Delrue
 * 06/10/2021
 */
public class PriorityQueue<T> implements FIFOQueue<T> {
    private TreeMap<Integer, LinkedList<T>> data;

    public PriorityQueue() {
        this.data = new TreeMap<Integer, LinkedList<T>>(Collections.reverseOrder());
    }

    @Override
    public boolean enqueue(T element, int priority) {
        for (var entry : data.entrySet()){
            for (T t : entry.getValue()){
                if (t.equals(element)){
                    return false;
                }
            }
        }

        if (data.get(priority) == null) {
            data.put(priority, new LinkedList<>());
            LinkedList<T> t = data.get(priority);
            t.add(element);
        } else {
            LinkedList<T> t = data.get(priority);
            t.add(element);
        }
        return true;
    }

    @Override
    public T dequeue() {

        for (var entry : data.entrySet()){
            for (T t : entry.getValue()){
                entry.getValue().remove(t);
                return t;
            }
        }

        return null;
    }

    @Override
    public int search(T element) {

        int result = 1;

        for (var entry : data.entrySet()){
            for (T t : entry.getValue()){
                if (t.equals(element)){
                    return result;
                } else {
                    result++;
                }
            }
        }
        return -1;
    }

    @Override
    public int getSize() {
        int result = 0;
        for (var entry : data.entrySet()){
            for (T t : entry.getValue()){
                result++;
            }
        }
        return  result;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        // treemap overlopen
        Set<Integer> keySet = data.keySet();

        for(Integer key : keySet) {
            if (data.get(key).getClass() == LinkedList.class){
                LinkedList<T> list = data.get(key);
                for (T t : list){
                    result.append(String.format("%d: %s\n", key, t.toString()));
                }
            }
        }
        return result.toString();
    }
}

