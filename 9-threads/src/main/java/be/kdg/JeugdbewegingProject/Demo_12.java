package be.kdg.JeugdbewegingProject;

import be.kdg.JeugdbewegingProject.model.JeugdbewegingFactory;
import be.kdg.JeugdbewegingProject.model.Jeugdbewegingen;

import java.util.stream.Stream;

/**
 * Tim Delrue
 * 22/01/2022
 */
public class Demo_12 {
    public static void main(String[] args) {
        Jeugdbewegingen jeugdbewegingen = new Jeugdbewegingen(10000);

        Thread thread1 = new Thread(() -> Stream.generate(JeugdbewegingFactory::newRandomJeugdbeweging).limit(5000).forEach(jeugdbewegingen::add));
        Thread thread2 = new Thread(() -> Stream.generate(JeugdbewegingFactory::newRandomJeugdbeweging).limit(5000).forEach(jeugdbewegingen::add));

        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Na toevoegen door 2 threads met elk 5000 objecten: jeugdbewegingen = " + jeugdbewegingen.getSize());
    }
}
