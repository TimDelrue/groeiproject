package be.kdg.JeugdbewegingProject;

import be.kdg.JeugdbewegingProject.model.Type;
import be.kdg.JeugdbewegingProject.threading.JeugdbewegingRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;

/**
 * Tim Delrue
 * 22/01/2022
 */
public class Demo_9 {
    private static int TESTCOUNT = 1;

    public static void main(String[] args) {

        List<Long> timeList = new ArrayList<Long>();

        while (TESTCOUNT < 101) {
            JeugdbewegingRunnable runnable1 = new JeugdbewegingRunnable(jeugdbeweging -> jeugdbeweging.getAantalLeden() > 80);
            JeugdbewegingRunnable runnable2 = new JeugdbewegingRunnable(jeugdbeweging -> jeugdbeweging.getAantalLeiding() < 14);
            JeugdbewegingRunnable runnable3 = new JeugdbewegingRunnable(jeugdbeweging -> jeugdbeweging.getType() == Type.SCOUTS);

            Thread thread1 = new Thread(runnable1, "thread1");
            Thread thread2 = new Thread(runnable2, "thread2");
            Thread thread3 = new Thread(runnable3, "thread3");

            long startTime = System.currentTimeMillis();

            thread1.start();
            thread2.start();
            thread3.start();

            try {
                thread1.join();
                thread2.join();
                thread3.join();

                runnable1.getJeugdbewegingList().stream().limit(5).forEach(System.out::println);
                runnable2.getJeugdbewegingList().stream().limit(5).forEach(System.out::println);
                runnable3.getJeugdbewegingList().stream().limit(5).forEach(System.out::println);

                System.out.println("Time for all threads = " + (System.currentTimeMillis() - startTime) + " millis");
                timeList.add(System.currentTimeMillis() - startTime);
                TESTCOUNT++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        double average = 0;
        average = timeList.stream().mapToDouble(value -> value).average().getAsDouble();
        System.out.printf("3 threads verzamelen elk 1000 dictators (gemiddeld uit 100 runs): %.0f ms",average);
    }
}
