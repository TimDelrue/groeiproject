package be.kdg.JeugdbewegingProject;

import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import be.kdg.JeugdbewegingProject.model.Type;
import be.kdg.JeugdbewegingProject.threading.JeugdbewegingCallable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Tim Delrue
 * 22/01/2022
 */
public class Demo_11 {
    private static int TESTCOUNT = 1;

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(3);

        List<Integer> threadsTimes = new ArrayList<>();

        while (TESTCOUNT < 101) {
            Long startTime = System.currentTimeMillis();
            Callable<List<Jeugdbeweging>> callable1 = new JeugdbewegingCallable(jeugdbeweging -> jeugdbeweging.getAantalLeden() > 80);
            Callable<List<Jeugdbeweging>> callable2 = new JeugdbewegingCallable(jeugdbeweging -> jeugdbeweging.getAantalLeiding() < 14);
            Callable<List<Jeugdbeweging>> callable3 = new JeugdbewegingCallable(jeugdbeweging -> jeugdbeweging.getType() == Type.SCOUTS);

            executorService.submit(callable1);
            executorService.submit(callable2);
            executorService.submit(callable3);

            Future<List<Jeugdbeweging>> future1 = executorService.submit(callable1);
            Future<List<Jeugdbeweging>> future2 = executorService.submit(callable2);
            Future<List<Jeugdbeweging>> future3 = executorService.submit(callable3);

            while (!future3.isDone() && !future2.isDone() && !future1.isDone()) {
                try {
                    Thread.sleep(40);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            try {
                executorService.awaitTermination(0, TimeUnit.MINUTES);
                threadsTimes.add((int) (System.currentTimeMillis() - startTime));
                TESTCOUNT++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        executorService.shutdown();
        System.out.println("3 Futures verzamelen elk 1000 Jeugdbewegingen (gemiddeld uit 100 runs) in: " + threadsTimes.stream().mapToDouble(Integer::doubleValue).average().getAsDouble() + " ms");
    }
}
