package be.kdg.JeugdbewegingProject.threading;

import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import be.kdg.JeugdbewegingProject.model.JeugdbewegingFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Tim Delrue
 * 22/01/2022
 */
public class JeugdbewegingCallable implements Callable {
    private List<Jeugdbeweging> jeugdbewegingen = new ArrayList<>();
    private final Predicate<Jeugdbeweging> jeugdbewegingPredicate;

    public JeugdbewegingCallable(Predicate<Jeugdbeweging> jeugdbewegingPredicate) {
        this.jeugdbewegingPredicate = jeugdbewegingPredicate;
    }

    @Override
    public Object call() throws Exception {
        return Stream.generate(JeugdbewegingFactory::newRandomJeugdbeweging).filter(jeugdbewegingPredicate).limit(1000).collect(Collectors.toList());
    }
}
