package be.kdg.JeugdbewegingProject.threading;

import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;

import java.util.List;
import java.util.function.Predicate;

/**
 * Tim Delrue
 * 22/01/2022
 */
public class JeugdbewegingAttacker implements Runnable {
    private List<Jeugdbeweging> jeugdbewegingList;
    private Predicate<Jeugdbeweging> jeugdbewegingPredicate;

    public JeugdbewegingAttacker(List<Jeugdbeweging> jeugdbewegingList, Predicate<Jeugdbeweging> jeugdbewegingPredicate) {
        this.jeugdbewegingPredicate = jeugdbewegingPredicate;
        this.jeugdbewegingList = jeugdbewegingList;
    }

    @Override
    public void run() {
        synchronized (jeugdbewegingList){
            jeugdbewegingList.removeIf(jeugdbewegingPredicate);
        }
    }
}
