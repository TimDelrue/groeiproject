package be.kdg.JeugdbewegingProject.threading;

import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import be.kdg.JeugdbewegingProject.model.JeugdbewegingFactory;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Tim Delrue
 * 22/01/2022
 */
public class JeugdbewegingRunnable implements Runnable{
    private Predicate<Jeugdbeweging> predicate;
    private List<Jeugdbeweging> jeugdbewegingList;

    public JeugdbewegingRunnable() {
    }

    public JeugdbewegingRunnable(Predicate<Jeugdbeweging> predicate) {
        this.predicate = predicate;
    }

    @Override
    public void run() {
        jeugdbewegingList = Stream.generate(JeugdbewegingFactory::newRandomJeugdbeweging).filter(predicate).limit(1000).collect(Collectors.toList());
    }

    public List<Jeugdbeweging> getJeugdbewegingList() {
        return jeugdbewegingList;
    }
}
