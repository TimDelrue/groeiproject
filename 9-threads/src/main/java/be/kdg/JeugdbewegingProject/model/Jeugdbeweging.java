package be.kdg.JeugdbewegingProject.model;

import java.time.LocalDate;

/**
 * Tim Delrue
 * 05/10/2021
 */
public final class Jeugdbeweging implements Comparable<Jeugdbeweging> {
    private final String naam;
    private final double oppervlakte;
    private final int aantalLeden;
    private final int aantalLeiding;
    private final Type type;
    private final LocalDate datumOprichting;

    public Jeugdbeweging() {
        this.naam = "Onbekend";
        this.oppervlakte = 100;
        this.aantalLeden = 100;
        this.aantalLeiding = 10;
        this.type = Type.SCOUTS;
        this.datumOprichting = LocalDate.of(2000,1,1);
    }

    public Jeugdbeweging(String naam, double oppervlakte, int aantalLeden, int aantalLeiding, Type type, LocalDate datumOprichting) {
        this.naam = naam;
        this.oppervlakte = oppervlakte;
        this.aantalLeden = aantalLeden;
        this.aantalLeiding = aantalLeiding;
        this.type = type;
        this.datumOprichting = datumOprichting;
    }

    public String getNaam() {
        return naam;
    }

    public double getOppervlakte() {
        return oppervlakte;
    }


    public int getAantalLeden() {
        return aantalLeden;
    }


    public int getAantalLeiding() {
        return aantalLeiding;
    }


    public Type getType() {
        return type;
    }


    public LocalDate getDatumOprichting() {
        return datumOprichting;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Jeugdbeweging that = (Jeugdbeweging) o;

        return naam != null ? naam.equals(that.naam) : that.naam == null;
    }

    @Override
    public int hashCode() {
        return naam != null ? naam.hashCode() : 0;
    }

    @Override
    public int compareTo(Jeugdbeweging o) {
        return this.naam.compareTo(o.naam);
    }

    @Override
    public String toString() {
        return String.format("naam: %20s, oppervlakte: %8.2f, aantal leden: %5d, aantal leiding: %5d, type: %10s, datum oprichting: %s",this.naam,this.oppervlakte,this.aantalLeden,this.aantalLeiding,this.type,this.datumOprichting.toString());
    }
}
