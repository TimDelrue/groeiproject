package be.kdg.JeugdbewegingProject.model;

import java.time.LocalDate;
import java.util.Random;

/**
 * Tim Delrue
 * 25/11/2021
 */
public class JeugdbewegingFactory {
    private JeugdbewegingFactory() {
    }

    public static Jeugdbeweging newEmptyJeugdbeweging() {
        return new Jeugdbeweging();
    }

    public static Jeugdbeweging newFilledJeugdbeweging(String naam, double oppervlakte, int aantalLeden, int aantalLeiding, Type type, LocalDate datumOprichting) {
        return new Jeugdbeweging(naam, oppervlakte, aantalLeden, aantalLeiding, type, datumOprichting);
    }

    public static Jeugdbeweging newRandomJeugdbeweging() {
        Random random = new Random();
        Jeugdbeweging result = new Jeugdbeweging(
                generateString(10, 2, false),
                random.nextDouble() * random.nextInt(100) + 100,
                random.nextInt(50) + 40,
                random.nextInt(20) + 12,
                Type.values()[random.nextInt(5)],
                LocalDate.of(random.nextInt(120) + 1901, random.nextInt(12) + 1, random.nextInt(28) + 1));
        return result;
    }

    private static String generateString(int maxWordLength, int WordCount, boolean camelCase) {
        Random random = new Random();
        StringBuilder naam = new StringBuilder();
        char[] klinkers = {'a', 'e', 'i', 'o', 'u'};
        char[] medeklinkers = {'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'};
        for (int j = 0; j < WordCount; j++) {
            for (int i = 0; i < random.nextInt(maxWordLength - 3) + 4; i++) {
                if (random.nextInt(3) == 0) {
                    // klinker
                    naam.append(klinkers[random.nextInt(5)]);
                } else {
                    naam.append(medeklinkers[random.nextInt(21)]);
                }
            }
            if (j + 1 != WordCount) {
                naam.append(" ");
            }
        }
        if (camelCase) {
            String[] splitNames = naam.toString().split(" ");
            naam.setLength(0);
            for (String splitName : splitNames) {
                naam.append(splitName.substring(0, 1).toUpperCase() + splitName.substring(1));
            }
        }

        return naam.toString();
    }
}
