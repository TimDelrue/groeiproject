package be.kdg.JeugdbewegingProject;

import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import be.kdg.JeugdbewegingProject.model.JeugdbewegingFactory;
import be.kdg.JeugdbewegingProject.model.Type;
import be.kdg.JeugdbewegingProject.threading.JeugdbewegingAttacker;
import be.kdg.JeugdbewegingProject.threading.JeugdbewegingRunnable;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Tim Delrue
 * 22/01/2022
 */
public class Demo_10 {
    public static void main(String[] args) {
        List<Jeugdbeweging> jeugdbewegingen = Stream.generate(JeugdbewegingFactory::newRandomJeugdbeweging).limit(1000).collect(Collectors.toList());
        JeugdbewegingAttacker runnable1 = new JeugdbewegingAttacker(jeugdbewegingen, jeugdbeweging -> jeugdbeweging.getAantalLeden() > 80);
        JeugdbewegingAttacker runnable2 = new JeugdbewegingAttacker(jeugdbewegingen, jeugdbeweging -> jeugdbeweging.getAantalLeiding() < 14);
        JeugdbewegingAttacker runnable3 = new JeugdbewegingAttacker(jeugdbewegingen, jeugdbeweging -> jeugdbeweging.getType() == Type.SCOUTS);

        Thread thread1 = new Thread(runnable1, "thread1");
        Thread thread2 = new Thread(runnable2, "thread2");
        Thread thread3 = new Thread(runnable3, "thread3");

        thread1.start();
        thread2.start();
        thread3.start();

        try{
            thread1.join();
            thread2.join();
            thread3.join();

            System.out.println("Na zuivering:");
            System.out.println("Aantal met meer dan 80 leden: " + jeugdbewegingen.stream().filter(jeugdbeweging -> jeugdbeweging.getAantalLeden() > 80).count());
            System.out.println("Aantal met minder dan 14 leiding: " + jeugdbewegingen.stream().filter(jeugdbeweging -> jeugdbeweging.getAantalLeiding() < 14).count());
            System.out.println("Aantal met als type scouts: " + jeugdbewegingen.stream().filter(jeugdbeweging -> jeugdbeweging.getType() == Type.SCOUTS).count());
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }



}
