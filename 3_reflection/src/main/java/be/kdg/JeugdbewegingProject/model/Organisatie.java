package be.kdg.JeugdbewegingProject.model;

import be.kdg.JeugdbewegingProject.reflection.CanRun;

import java.time.LocalDate;

/**
 * Tim Delrue
 * 18/10/2021
 */
public class Organisatie {
    protected String naam;
    protected double oppervlakte;
    protected LocalDate datumOprichting;

    public Organisatie() {
        this("onbekend",100,LocalDate.of(2000,1,1));
    }

    public Organisatie(String naam, double oppervlakte, LocalDate datumOprichting) {
        this.setNaam(naam);
        this.oppervlakte = oppervlakte;
        this.datumOprichting = datumOprichting;
    }

    public String getNaam() {
        return naam;
    }

    @CanRun("PlePlo")
    public void setNaam(String naam) {
        this.naam = naam;
    }

    public double getOppervlakte() {
        return oppervlakte;
    }

    public void setOppervlakte(double oppervlakte) throws IllegalArgumentException {
        if (oppervlakte < 10 || oppervlakte > 10000) {
            throw new IllegalArgumentException("De oppervlakte moet tussen 10 en 10000 M² liggen.");
        }
        this.oppervlakte = oppervlakte;
    }

    public LocalDate getDatumOprichting() {
        return datumOprichting;
    }

    public void setDatumOprichting(LocalDate datumOprichting) {
        if (datumOprichting.isBefore(LocalDate.of(1900, 1, 1)) || datumOprichting.isAfter(LocalDate.now())) {
            throw new IllegalArgumentException("De oprichtings datum kan niet voor 1900 of na de datum van vandaag vallen.");
        }
        this.datumOprichting = datumOprichting;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Jeugdbeweging that = (Jeugdbeweging) o;

        return naam != null ? naam.equals(that.naam) : that.naam == null;
    }

    @Override
    public int hashCode() {
        return naam != null ? naam.hashCode() : 0;
    }

    public int compareTo(Organisatie o) {
        return this.naam.compareTo(o.naam);
    }

    @Override
    public String toString() {
            return String.format("naam: %20s, oppervlakte: %8.2f, datum oprichting: %s",this.naam,this.oppervlakte,this.datumOprichting.toString());
    }
}
