package be.kdg.JeugdbewegingProject.model;

/**
 * Tim Delrue
 * 05/10/2021
 */
public enum Type {
    SCOUTS, CHIRO, KSA, KLJ, ZEESCOUTS;

    @Override
    public String toString() {
        switch (this) {
            case SCOUTS:
                return "scouts";

            case CHIRO:
                return "chiro";

            case KSA:
                return "ksa";

            case KLJ:
                return "klj";

            case ZEESCOUTS:
                return "zeescouts";

            default:
                return "unknown";


        }
    }
}
