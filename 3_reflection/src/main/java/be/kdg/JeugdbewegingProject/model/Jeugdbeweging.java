package be.kdg.JeugdbewegingProject.model;

import java.time.LocalDate;

/**
 * Tim Delrue
 * 05/10/2021
 */
public class Jeugdbeweging extends Organisatie implements Comparable<Jeugdbeweging> {
    private int aantalLeden;
    private int aantalLeiding;
    private Type type;

    public Jeugdbeweging() {
        super();
        this.aantalLeden = 100;
        this.aantalLeiding = 10;
        this.type = Type.SCOUTS;
    }

    public Jeugdbeweging(String naam, double oppervlakte, int aantalLeden, int aantalLeiding, Type type, LocalDate datumOprichting) {
        super(naam,oppervlakte,datumOprichting);
        this.setAantalLeden(aantalLeden);
        this.setAantalLeiding(aantalLeiding);
        this.setType(type);
    }

    public int getAantalLeden() {
        return aantalLeden;
    }

    public void setAantalLeden(int aantalLeden) {
        if (aantalLeden < 10 || aantalLeden > 2500){
            throw new IllegalArgumentException("Het aantal leden moet tussen de 10 en 2500 liggen");
        }
        this.aantalLeden = aantalLeden;
    }

    public int getAantalLeiding() {
        return aantalLeiding;
    }

    public void setAantalLeiding(int aantalLeiding) {
        if (aantalLeiding < 10 || aantalLeiding > 500){
            throw new IllegalArgumentException("Het aantal leiding moet tussen de 10 en 500 liggen");
        }
        this.aantalLeiding = aantalLeiding;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return String.format("%s, aantal leden: %5d, aantal leiding: %5d, type: %10s",super.toString(),this.aantalLeden,this.aantalLeiding,this.type);
    }

    @Override
    public int compareTo(Jeugdbeweging o) {
        return super.compareTo(o);
    }
}
