package be.kdg.JeugdbewegingProject.reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;

/**
 * Tim Delrue
 * 18/10/2021
 */
public class ReflectionTools {
    public static void classAnalysis(Class... classes) {
        // nergens casten!

        /*
        volledige klassenaam
        naam van de superklasse
        naam van de package
        gebruikte inferfaces
        constructors (constructor.toGenericString())
        private attributen
        alle methoden
         */
        for (Class aClass : classes) {
            System.out.printf("Analyse van de klasse: %s\n", aClass.getSimpleName());
            System.out.println("===============================================");
            System.out.printf("Fully qualified name    : %s\n", aClass.getName());
            System.out.printf("Name van de superklasse : %s\n", aClass.getSuperclass().getSimpleName());
            System.out.printf("Naam van de package     : %s\n", aClass.getPackage());
            StringBuilder interfaces = new StringBuilder();
            for (Class c : aClass.getInterfaces()) {
                interfaces.append(String.format("%s, ", c.getSimpleName()));
            }
            if (interfaces.length() > 1)
                interfaces = new StringBuilder(interfaces.substring(0, interfaces.length() - 2));
            System.out.printf("Interfaces: %s\n", interfaces);

            StringBuilder constructors = new StringBuilder();
            for (Constructor c : aClass.getConstructors()) {
                constructors.append(String.format("%s, ", c.toString()));
            }
            if (constructors.length() > 1)
                constructors = new StringBuilder(constructors.substring(0, constructors.length() - 2));
            System.out.printf("constructors    : %s\n", constructors);

            StringBuilder attributen = new StringBuilder();
            for (Field f : aClass.getDeclaredFields()) {
                attributen.append(String.format("%s(%s), ", f.getName(), f.getType().getSimpleName()));
            }
            if (attributen.length() > 1)
                attributen = new StringBuilder(attributen.substring(0, attributen.length() - 2));
            System.out.printf("attributen      : %s\n", attributen);

            StringBuilder getters = new StringBuilder();
            for (Method m : aClass.getDeclaredMethods()) {
                if (m.getName().startsWith("get")) {
                    getters.append(String.format("%s, ", m.getName()));
                }
            }
            if (getters.length() > 1) getters = new StringBuilder(getters.substring(0, getters.length() - 2));
            System.out.printf("getters         : %s\n", getters);

            StringBuilder setters = new StringBuilder();
            for (Method m : aClass.getDeclaredMethods()) {
                if (m.getName().startsWith("set")) {
                    setters.append(String.format("%s, ", m.getName()));
                }
            }
            if (setters.length() > 1) setters = new StringBuilder(setters.substring(0, setters.length() - 2));
            System.out.printf("setters         : %s\n", setters);

            StringBuilder otherMethods = new StringBuilder();
            for (Method m : aClass.getDeclaredMethods()) {
                if (!(m.getName().startsWith("set") || m.getName().startsWith("get"))) {
                    otherMethods.append(String.format("%s, ", m.getName()));
                }
            }
            if (otherMethods.length() > 1)
                otherMethods = new StringBuilder(otherMethods.substring(0, otherMethods.length() - 2));
            System.out.printf("andere methoden : %s\n\n", otherMethods);
        }
    }

    public static Object runAnnotated(Class aClass) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {

        Object object = aClass.getDeclaredConstructor().newInstance();

        for (Method method : aClass.getMethods()) {
                if (method.getAnnotation(CanRun.class) != null) {
                    if (method.getParameterCount() == 1) {
                        if (method.getParameterTypes()[0].equals(String.class)) {
                            method.invoke(object, method.getAnnotation(CanRun.class).value());
                        }
                    }
            }
        }
        return object;
    }
}
