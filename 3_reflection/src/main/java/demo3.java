import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import be.kdg.JeugdbewegingProject.model.Jeugdbewegingen;
import be.kdg.JeugdbewegingProject.model.Organisatie;
import be.kdg.JeugdbewegingProject.reflection.ReflectionTools;

import java.lang.reflect.InvocationTargetException;

/**
 * Tim Delrue
 * 18/10/2021
 */
public class demo3 {
    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        ReflectionTools.classAnalysis(Jeugdbeweging.class, Organisatie.class, Jeugdbewegingen.class);
        System.out.println(ReflectionTools.runAnnotated(Jeugdbeweging.class).toString());
    }
}
