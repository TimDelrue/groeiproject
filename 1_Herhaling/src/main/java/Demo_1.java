import be.kdg.JeugdbewegingProject.data.Data;
import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import be.kdg.JeugdbewegingProject.model.Jeugdbewegingen;
import be.kdg.JeugdbewegingProject.model.Type;

import java.time.LocalDate;

/**
 * Tim Delrue
 * 05/10/2021
 */
public class Demo_1 {
    public static void main(String[] args) {
        Jeugdbewegingen jeugdbewegingen = new Jeugdbewegingen();
        for (Jeugdbeweging jeugdbeweging : Data.getData()){
            jeugdbewegingen.add(jeugdbeweging);
        }
        Jeugdbeweging dubbel = new Jeugdbeweging("pleplo", 123.4,75,14, Type.CHIRO, LocalDate.of(1997,1,1));
        jeugdbewegingen.add(dubbel);

        jeugdbewegingen.search("pleplo");
        jeugdbewegingen.remove("pleplo");
        System.out.println(jeugdbewegingen.getSize());

        System.out.println("Sorteren op naam:");
        for (Jeugdbeweging jeugdbeweging : jeugdbewegingen.sortedOnName()){
            System.out.println(jeugdbeweging.toString());
        }
        System.out.println("\nSorteren op oppervlakte:");
        for (Jeugdbeweging jeugdbeweging : jeugdbewegingen.sortedOnSurface()){
            System.out.println(jeugdbeweging.toString());
        }
        System.out.println("\nSorteren op aantal leden:");
        for (Jeugdbeweging jeugdbeweging : jeugdbewegingen.sortedOnMemberCount()){
            System.out.println(jeugdbeweging.toString());
        }
    }
}
