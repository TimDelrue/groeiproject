package be.kdg.JeugdbewegingProject.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;

/**
 * Tim Delrue
 * 05/10/2021
 */
@XmlRootElement(name = "jeugdbewegingen")
public class Jeugdbewegingen {
    private ArrayList<Jeugdbeweging> jeugdbewegingen;

    public boolean add(Jeugdbeweging jeugdbeweging){
        return jeugdbewegingen.add(jeugdbeweging);
    }

    public boolean remove(String naam){
        Iterator iterator = jeugdbewegingen.iterator();
        boolean isSuccesfull = false;

        while (iterator.hasNext()){
            Jeugdbeweging currJeugdbeweging = (Jeugdbeweging) iterator.next();
            if(currJeugdbeweging.getNaam().equals(naam)){
                iterator.remove();
                isSuccesfull = true;
            }
        }
        return isSuccesfull;
    }

    public Jeugdbeweging search(String naam){
        Iterator iterator = jeugdbewegingen.iterator();
        boolean isSuccesfull = false;

        while (iterator.hasNext()){
            Jeugdbeweging currJeugdbeweging = (Jeugdbeweging) iterator.next();
            if(currJeugdbeweging.getNaam().equals(naam)){
                return currJeugdbeweging;
            }
        }
        return null;
    }

    public List<Jeugdbeweging> sortedOnName(){
        List<Jeugdbeweging> result = new ArrayList<>();
        for (Object jb : jeugdbewegingen.toArray())
        {
            result.add((Jeugdbeweging) jb);
        }

        Collections.sort(result, new Comparator<Jeugdbeweging>(){
            @Override
            public int compare(Jeugdbeweging o1, Jeugdbeweging o2) {
                return o1.getNaam().compareTo(o2.getNaam());
            }
        });
        return result;
    }

    public List<Jeugdbeweging> sortedOnMemberCount(){
        List<Jeugdbeweging> result = new ArrayList<>();
        for (Object jb : jeugdbewegingen.toArray())
        {
            result.add((Jeugdbeweging) jb);
        }
        Collections.sort(result, new Comparator<Jeugdbeweging>(){
            @Override
            public int compare(Jeugdbeweging o1, Jeugdbeweging o2) {
                return o1.getAantalLeden() - o2.getAantalLeden();
            }
        });
        return result;
    }

    public List<Jeugdbeweging> sortedOnSurface(){
        List<Jeugdbeweging> result = new ArrayList<>();
        for (Object jb : jeugdbewegingen.toArray())
        {
            result.add((Jeugdbeweging) jb);
        }
        Collections.sort(result, new Comparator<Jeugdbeweging>(){
            @Override
            public int compare(Jeugdbeweging o1, Jeugdbeweging o2) {
                return (int) (o1.getOppervlakte() - o2.getOppervlakte());
            }
        });
        return result;
    }

    public int getSize(){
        return jeugdbewegingen.size();
    }

    public ArrayList<Jeugdbeweging> getJeugdbewegingen() {
        return jeugdbewegingen;
    }

    @XmlElement(name = "jeugdbeweging")
    public void setJeugdbewegingen(ArrayList<Jeugdbeweging> jeugdbewegingen) {
        this.jeugdbewegingen = jeugdbewegingen;
    }

    public Jeugdbewegingen() {
        this.jeugdbewegingen = new ArrayList<Jeugdbeweging>();
    }
}
