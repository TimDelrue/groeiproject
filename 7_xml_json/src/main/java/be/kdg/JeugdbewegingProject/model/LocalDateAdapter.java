package be.kdg.JeugdbewegingProject.model;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Tim Delrue
 * 11/01/2022
 */
public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {
    public LocalDate unmarshal(String myString) throws Exception {
        return LocalDate.parse(myString, DateTimeFormatter.ofPattern("MM/dd/yyyy"));
    }
    public String marshal(LocalDate myDate) throws Exception {
        return myDate.format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
    }
}

