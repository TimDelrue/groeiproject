package be.kdg.JeugdbewegingProject.parsing;

import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 * Tim Delrue
 * 11/01/2022
 */
public class JeugdbewegingenJaxbParser {

    public static void JaxbWriteXml(String file, Object root) {
        try {
            JAXBContext context = JAXBContext.newInstance(root.getClass());
            Marshaller m = null;
            m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            m.marshal(root, new File(file));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public static <T> T JaxbReadXml(String file, Class<T> typeParameterClass){
        T result = null;
        try {
            JAXBContext jc = JAXBContext.newInstance(typeParameterClass);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            File f = new File(file);
            result = (T) unmarshaller.unmarshal(f);
        }
        catch (JAXBException e) {
            e.printStackTrace();
        }
        return result;
    }

}
