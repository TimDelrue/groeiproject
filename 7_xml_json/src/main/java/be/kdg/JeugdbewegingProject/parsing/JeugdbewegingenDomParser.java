package be.kdg.JeugdbewegingProject.parsing;

import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import be.kdg.JeugdbewegingProject.model.Jeugdbewegingen;
import be.kdg.JeugdbewegingProject.model.Type;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Tim Delrue
 * 11/01/2022
 */
public class JeugdbewegingenDomParser {

    public static Jeugdbewegingen domReadXML(String fileName) {
        Jeugdbewegingen result = new Jeugdbewegingen();
        try {
            XMLEventReader eventReader = XMLInputFactory.newInstance()
                    .createXMLEventReader(new FileReader(fileName, StandardCharsets.UTF_8));

            String tagCurrentElement = "";
            boolean currentTagElementCleared = false;
            String naamJeugdbeweging = "";
            double oppervlakteJeugdbeweging = 0;
            int aantalLedenJeugdbeweging = 0;
            int aantalLeidingJeugdbeweging = 0;
            Type typeJeugdbeweging = null;
            LocalDate datumOprichtingJeugdbeweging = null;

            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();

                switch (event.getEventType()) {
                    case XMLStreamConstants.START_ELEMENT:
                        StartElement startElement = event.asStartElement();
                        tagCurrentElement = startElement.getName().getLocalPart();
                        currentTagElementCleared = false;
                        break;

                    case XMLStreamConstants.CHARACTERS:
                        if (!currentTagElementCleared) {
                            Characters characters = event.asCharacters();
                            switch (tagCurrentElement) {
                                case "naam":
                                    naamJeugdbeweging = characters.getData();
                                    break;
                                case "oppervlakte":
                                    oppervlakteJeugdbeweging = Double.parseDouble(characters.getData());
                                    break;
                                case "aantalLeden":
                                    aantalLedenJeugdbeweging = Integer.parseInt(characters.getData());
                                    break;
                                case "aantalLeiding":
                                    aantalLeidingJeugdbeweging = Integer.parseInt(characters.getData());
                                    break;
                                case "type":
                                    String tmpType = characters.getData();
                                    for (Type type : Type.values()) {
                                        if (type.toString().toUpperCase().equals(tmpType.toUpperCase())) {
                                            typeJeugdbeweging = type;
                                        }
                                    }
                                    break;
                                case "datumOprichting":
                                    datumOprichtingJeugdbeweging = LocalDate.parse(characters.getData(),
                                            DateTimeFormatter.ofPattern("MM/dd/yyyy"));

                                    // jeugdbeweging opslaan
                                    result.add(new Jeugdbeweging(naamJeugdbeweging, oppervlakteJeugdbeweging, aantalLedenJeugdbeweging,
                                            aantalLeidingJeugdbeweging, typeJeugdbeweging, datumOprichtingJeugdbeweging));
                                    break;
                            }
                            currentTagElementCleared = true;
                        }
                        break;
                }
            }
        } catch (IOException | XMLStreamException e) {
            e.printStackTrace();
        }

        return result;
    }
}
