package be.kdg.JeugdbewegingProject.parsing;

import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import be.kdg.JeugdbewegingProject.model.Jeugdbewegingen;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.Arrays;
import java.util.List;

/**
 * Tim Delrue
 * 11/01/2022
 */
public class JeugdbewegingenGsonParser {
    public static void writeJson(Jeugdbewegingen jeugdbewegingen, String fileName){
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.setPrettyPrinting().create();

        String jsonString = gson.toJson(jeugdbewegingen);
        try (FileWriter jsonWriter =
                     new FileWriter(fileName)) {
            jsonWriter.write(jsonString);
        }catch(IOException e) {
            e.printStackTrace();
        }
    }

    public static Jeugdbewegingen readJson(String fileName){
        try (BufferedReader data = new BufferedReader(new FileReader(fileName))) {
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();

            Jeugdbewegingen jeugdbewegingen = gson.fromJson(data, Jeugdbewegingen.class);
            return jeugdbewegingen;
        }  catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
