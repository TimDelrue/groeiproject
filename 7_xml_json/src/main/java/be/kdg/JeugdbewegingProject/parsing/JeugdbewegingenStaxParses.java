package be.kdg.JeugdbewegingProject.parsing;

import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import be.kdg.JeugdbewegingProject.model.Jeugdbewegingen;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.format.DateTimeFormatter;

/**
 * Tim Delrue
 * 10/01/2022
 */
public class JeugdbewegingenStaxParses {
    Jeugdbewegingen jeugdbewegingen;
    XMLStreamWriter xmlStreamWriter;

    public JeugdbewegingenStaxParses(Jeugdbewegingen jeugdbewegingen, String path) {
        this.jeugdbewegingen = jeugdbewegingen;

        try {
            FileWriter file = new FileWriter(path, StandardCharsets.UTF_8);

            xmlStreamWriter = new IndentingXMLStreamWriter(XMLOutputFactory.newInstance().createXMLStreamWriter(file));

        } catch (IOException | XMLStreamException e) {
            e.printStackTrace();
        }
    }

    public void staxWriteXML() {
        try {
            xmlStreamWriter.writeStartDocument();
            xmlStreamWriter.writeStartElement("jeugdbewegingen");

            for(Jeugdbeweging jeugdbeweging : jeugdbewegingen.sortedOnName()){
                writeElement(jeugdbeweging);
            }

            xmlStreamWriter.writeEndElement();
            xmlStreamWriter.writeEndDocument(); // closes any unclosed tags
            xmlStreamWriter.close();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }

    private void writeElement(Jeugdbeweging jeugdbeweging) {

        try {
            xmlStreamWriter.writeStartElement("jeugdbeweging");
            xmlStreamWriter.writeAttribute("naam", jeugdbeweging.getNaam());

            writeElement("naam", jeugdbeweging.getNaam());
            writeElement("oppervlakte", Double.toString(jeugdbeweging.getOppervlakte()));
            writeElement("aantalLeden", Integer.toString(jeugdbeweging.getAantalLeden()));
            writeElement("aantalLeiding", Integer.toString(jeugdbeweging.getAantalLeiding()));
            writeElement("type", jeugdbeweging.getType().toString());
            writeElement("datumOprichting", jeugdbeweging.getDatumOprichting().format(
                    DateTimeFormatter.ofPattern("MM/dd/yyyy")
            ));
            xmlStreamWriter.writeEndElement(); // </person>
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }

    private void writeElement(String name, String value) throws XMLStreamException {
        xmlStreamWriter.writeStartElement(name);
        xmlStreamWriter.writeCharacters(value);
        xmlStreamWriter.writeEndElement();
    }

}
