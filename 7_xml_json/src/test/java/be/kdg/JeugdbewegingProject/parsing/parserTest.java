package be.kdg.JeugdbewegingProject.parsing;

import be.kdg.JeugdbewegingProject.data.Data;
import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import be.kdg.JeugdbewegingProject.model.Jeugdbewegingen;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tim Delrue
 * 11/01/2022
 */
public class parserTest {
    Jeugdbewegingen jeugdbewegingen;

    @BeforeEach
    public void setup(){
        jeugdbewegingen = new Jeugdbewegingen();
        for(Jeugdbeweging jeugdbeweging : Data.getData()){
            jeugdbewegingen.add(jeugdbeweging);
        }
    }

    @Test
    void testStaxDom(){
        JeugdbewegingenStaxParses jeugdbewegingenStaxParses =
                new JeugdbewegingenStaxParses(jeugdbewegingen,"datafiles/staxJeugdbewegingen.xml");

        jeugdbewegingenStaxParses.staxWriteXML();

        Jeugdbewegingen jeugdbewegingen2 = JeugdbewegingenDomParser.domReadXML("datafiles/staxJeugdbewegingen.xml");

        assertEquals(jeugdbewegingen.sortedOnName(),jeugdbewegingen2.sortedOnName(),"(Stax) De geschreven en terug opgehaalde jeugdbewegingen instanties zijn niet gelijk");
    }

    @Test
    void testJaxb(){
        JeugdbewegingenJaxbParser.JaxbWriteXml("datafiles/jaxbJeugdbewegingen.xml",jeugdbewegingen);

        Jeugdbewegingen jeugdbewegingen2 =
        JeugdbewegingenJaxbParser.JaxbReadXml("datafiles/jaxbJeugdbewegingen.xml", Jeugdbewegingen.class);

        assertEquals(jeugdbewegingen.sortedOnName(),jeugdbewegingen2.sortedOnName(),"(Jaxb) De geschreven en terug opgehaalde jeugdbewegingen instanties zijn niet gelijk");
    }

    @Test
    void testJson(){
        JeugdbewegingenGsonParser.writeJson(jeugdbewegingen,"datafiles/jeugdbewegingen.json");

        Jeugdbewegingen jeugdbewegingen2 =
                JeugdbewegingenGsonParser.readJson("datafiles/jeugdbewegingen.json");

        assertEquals(jeugdbewegingen.sortedOnName(),jeugdbewegingen2.sortedOnName(),"(Json) De geschreven en terug opgehaalde jeugdbewegingen instanties zijn niet gelijk");
    }
}
