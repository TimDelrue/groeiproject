package be.kdg.JeugdbewegingProject.model;

import java.io.Serial;
import java.io.Serializable;
import java.util.*;

/**
 * Tim Delrue
 * 05/10/2021
 */
public class Jeugdbewegingen implements Serializable {
    @Serial
    private static final long serialVersionUID = 0;

    private TreeSet treeSetJeugdbewegingen;

    public boolean add(Jeugdbeweging jeugdbeweging){
        return treeSetJeugdbewegingen.add(jeugdbeweging);
    }

    public boolean remove(String naam){
        Iterator iterator = treeSetJeugdbewegingen.iterator();
        boolean isSuccesfull = false;

        while (iterator.hasNext()){
            Jeugdbeweging currJeugdbeweging = (Jeugdbeweging) iterator.next();
            if(currJeugdbeweging.getNaam().equals(naam)){
                iterator.remove();
                isSuccesfull = true;
            }
        }
        return isSuccesfull;
    }

    public Jeugdbeweging search(String naam){
        Iterator iterator = treeSetJeugdbewegingen.iterator();
        boolean isSuccesfull = false;

        while (iterator.hasNext()){
            Jeugdbeweging currJeugdbeweging = (Jeugdbeweging) iterator.next();
            if(currJeugdbeweging.getNaam().equals(naam)){
                return currJeugdbeweging;
            }
        }
        return null;
    }

    public List<Jeugdbeweging> sortedOnName(){
        List<Jeugdbeweging> result = new ArrayList<>();
        for (Object jb : treeSetJeugdbewegingen.toArray())
        {
            result.add((Jeugdbeweging) jb);
        }

        Collections.sort(result, new Comparator<Jeugdbeweging>(){
            @Override
            public int compare(Jeugdbeweging o1, Jeugdbeweging o2) {
                return o1.getNaam().compareTo(o2.getNaam());
            }
        });
        return result;
    }

    public List<Jeugdbeweging> sortedOnMemberCount(){
        List<Jeugdbeweging> result = new ArrayList<>();
        for (Object jb : treeSetJeugdbewegingen.toArray())
        {
            result.add((Jeugdbeweging) jb);
        }
        Collections.sort(result, new Comparator<Jeugdbeweging>(){
            @Override
            public int compare(Jeugdbeweging o1, Jeugdbeweging o2) {
                return o1.getAantalLeden() - o2.getAantalLeden();
            }
        });
        return result;
    }

    public List<Jeugdbeweging> sortedOnSurface(){
        List<Jeugdbeweging> result = new ArrayList<>();
        for (Object jb : treeSetJeugdbewegingen.toArray())
        {
            result.add((Jeugdbeweging) jb);
        }
        Collections.sort(result, new Comparator<Jeugdbeweging>(){
            @Override
            public int compare(Jeugdbeweging o1, Jeugdbeweging o2) {
                return (int) (o1.getOppervlakte() - o2.getOppervlakte());
            }
        });
        return result;
    }

    public int getSize(){
        return treeSetJeugdbewegingen.size();
    }

    public Jeugdbewegingen() {
        this.treeSetJeugdbewegingen = new TreeSet<Jeugdbeweging>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Jeugdbewegingen that = (Jeugdbewegingen) o;

        return treeSetJeugdbewegingen != null ? treeSetJeugdbewegingen.equals(that.treeSetJeugdbewegingen) : that.treeSetJeugdbewegingen == null;
    }

    @Override
    public int hashCode() {
        return treeSetJeugdbewegingen != null ? treeSetJeugdbewegingen.hashCode() : 0;
    }
}
