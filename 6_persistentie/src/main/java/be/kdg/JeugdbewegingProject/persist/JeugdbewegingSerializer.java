package be.kdg.JeugdbewegingProject.persist;

import be.kdg.JeugdbewegingProject.model.Jeugdbewegingen;

import java.io.*;

/**
 * Tim Delrue
 * 07/01/2022
 */
public class JeugdbewegingSerializer {
    private final String FILENAME;

    public JeugdbewegingSerializer(String FILENAME) {
        this.FILENAME = FILENAME;
    }

    public void serialize(Jeugdbewegingen jeugdbewegingen) throws IOException {
        FileOutputStream fileOut = new FileOutputStream(FILENAME + ".ser");
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(jeugdbewegingen);
        out.close();
        fileOut.close();
    }

    public Jeugdbewegingen deserialize() throws IOException, ClassNotFoundException {
        FileInputStream fileIn = new FileInputStream("./db/ " + FILENAME + ".ser");

        ObjectInputStream in = new ObjectInputStream(fileIn);
        Jeugdbewegingen jeugdbewegingen = (Jeugdbewegingen) in.readObject();
        in.close();
        fileIn.close();

        return jeugdbewegingen;
    }

}
