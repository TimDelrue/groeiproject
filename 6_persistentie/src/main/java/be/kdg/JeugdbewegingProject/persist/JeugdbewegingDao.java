package be.kdg.JeugdbewegingProject.persist;

import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;

import java.util.List;

/**
 * Tim Delrue
 * 07/01/2022
 */
public interface JeugdbewegingDao {
    boolean insert(Jeugdbeweging jeugdbeweging);
    boolean delete(String naam);
    boolean update(Jeugdbeweging jeugdbeweging);
    Jeugdbeweging retrieve(String naam);
    List<Jeugdbeweging> sortedOn(String query);
}
