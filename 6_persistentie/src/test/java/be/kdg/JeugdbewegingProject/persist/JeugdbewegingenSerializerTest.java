package be.kdg.JeugdbewegingProject.persist;

import be.kdg.JeugdbewegingProject.data.Data;
import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import be.kdg.JeugdbewegingProject.model.Jeugdbewegingen;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tim Delrue
 * 07/01/2022
 */
public class JeugdbewegingenSerializerTest {
    JeugdbewegingSerializer jeugdbewegingenSerializer;
    Jeugdbewegingen jeugdbewegingen;
    @BeforeEach
    void setUp() {
        jeugdbewegingenSerializer = new JeugdbewegingSerializer("JeugdbewegingenDataBase");

        jeugdbewegingen = new Jeugdbewegingen();
        for (Jeugdbeweging j : Data.getData()) {
            jeugdbewegingen.add(j);
        }
    }

    @Test
    void testSerialize() {
        try{
            jeugdbewegingenSerializer.serialize(jeugdbewegingen);
        } catch (IOException e){
            fail("Serialize zou geen IOException mogen gooien.");
        }
    }

    @Test
    void testDeserialize(){

        try{
            Jeugdbewegingen jeugdbeweginenFromDeserialize = jeugdbewegingenSerializer.deserialize();
            if (!jeugdbeweginenFromDeserialize.equals(jeugdbewegingen)){
                fail("De opgehaalde data uit deserialize is niet dezelfde als de originele data");
            }
        } catch (Exception e){
            fail("Deserialize zou geen exception mogen gooien. Exception: " + e.getMessage());
        }
    }
}
