package be.kdg.JeugdbewegingProject.persist;

import be.kdg.JeugdbewegingProject.data.Data;
import be.kdg.JeugdbewegingProject.model.Jeugdbeweging;
import org.junit.jupiter.api.*;

import java.util.Comparator;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tim Delrue
 * 10/01/2022
 */
public class JeugdbewegingDbDaoTest {
    static JeugdbewegingDbDao jeugdbewegingDbDao;

    @BeforeAll
    static void initialSetup() {
        jeugdbewegingDbDao = new JeugdbewegingDbDao("./db");
    }

    @AfterAll
    static void afterAll() {
        jeugdbewegingDbDao.close();
    }

    @BeforeEach
    void fillDataBase() {
        for (Jeugdbeweging jeugdbeweging : Data.getData()) {
            jeugdbewegingDbDao.insert(jeugdbeweging);
        }
    }

    @AfterEach
    void deleteAllFromDb(){
        jeugdbewegingDbDao.delete("*");
    }

    @Test
    void testInsert(){
        assertEquals(jeugdbewegingDbDao.sortedOn("SELECT * from jeugdbewegingtable").size(), Data.getData().size(),
                "Het aantal records in de database komt niet overeen met het aantal records in de data klasse");
    }

    @Test
    void testRetrieveUpdate(){
        Jeugdbeweging jeugdbeweging = jeugdbewegingDbDao.retrieve("pleplo");
        jeugdbeweging.setNaam("plo ple");
        jeugdbewegingDbDao.update(jeugdbeweging);
        assertNotNull(jeugdbewegingDbDao.retrieve("plo ple"));
    }

    @Test
    void testDelete(){
        jeugdbewegingDbDao.delete("pleplo");
        assertEquals(jeugdbewegingDbDao.sortedOn("SELECT * from jeugdbewegingtable").size(), Data.getData().size()-1,
                "De jeugdbeweging 'pleplo' werd niet verwijderd");
        assertFalse(jeugdbewegingDbDao.delete("pleplo"),
                "Twee keer hetzelfde opbject verwijderen zou niet mogen lukken");
    }

    @Test
    void testSort(){
        assertEquals(Data.getData().stream().sorted(new Comparator<Jeugdbeweging>() {
            @Override
            public int compare(Jeugdbeweging o1, Jeugdbeweging o2) {
                return o1.getNaam().compareTo(o2.getNaam());
            }
        }).collect(Collectors.toList()),
                jeugdbewegingDbDao.sortedOnName(),
                "Sorteren op naam is niet gelukt.");
    }
}
